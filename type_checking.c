/**
 * @file type_checking.c
 * @author Karel Benes
 */

#include <stdlib.h>

#include "structures.h"
#include "type_checking.h"
#include "global.h"

/*
typedef enum  {
    VAR_UNDEFINED,
    VAR_NIL,
    VAR_BOOL,
    VAR_NUMERIC,
    VAR_STRING,
    VAR_ARRAY,
    VAR_FUNCNAME,
    VAR_UNKNOWN,
}variable_type;
*/

variable_type typeOfExpression(tExpr *expression)
{
    if(expression == NULL)
    {
        return VAR_UNDEFINED;
    }

    
    variable_type l_type,r_type,self;
    
    tExpr *param = expression->params;
    if(expression->type!=T_LITERAL && expression->type!=T_VARIABLE &&
        expression->type!=T_INTFUNC && expression->type!=T_FUNCCALL)
    {
        l_type = typeOfExpression(expression->data.children[0]);
        r_type = typeOfExpression(expression->data.children[1]);
    }
    switch(expression->type) 
    {
        case T_LITERAL:
            return (expression->data.value.type == VAR_FUNCNAME) ? \
                VAR_UNDEFINED : expression->data.value.type;
            break;
        case T_VARIABLE:
            return VAR_UNKNOWN;
            break;
        case T_PLUS:
            if(l_type == VAR_STRING && r_type != VAR_UNDEFINED)
            {
                return VAR_STRING;
            }
            else if(l_type ==VAR_ARRAY &&
                    (r_type ==VAR_ARRAY || r_type==VAR_UNKNOWN))
            {
                    return VAR_ARRAY;
            }
            else if(l_type ==VAR_NUMERIC &&
                    (r_type ==VAR_NUMERIC || r_type==VAR_UNKNOWN))
            {
                    return VAR_NUMERIC;
            }
            else if(l_type == VAR_UNKNOWN)
            {
                return VAR_UNKNOWN;
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_MINUS:
            if((l_type==VAR_NUMERIC || l_type==VAR_UNKNOWN) &&
                (r_type==VAR_NUMERIC || r_type==VAR_UNKNOWN))
            {
                return VAR_NUMERIC;
            }
            else
            {
                return VAR_UNDEFINED;
            }

            break;
        case T_ASTERISK:
            if(r_type==VAR_NUMERIC || r_type==VAR_UNKNOWN)
            {
                if(l_type==VAR_NUMERIC)
                {
                    return VAR_NUMERIC;
                }
                else if(l_type==VAR_STRING)
                {
                    return VAR_STRING;
                }
                else if(l_type==VAR_UNKNOWN)
                {
                    return VAR_UNKNOWN;
                }
                else
                {
                    return VAR_UNKNOWN;
                }
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_SLASH:
            if((l_type==VAR_NUMERIC || l_type==VAR_UNKNOWN) &&
                (r_type==VAR_NUMERIC || r_type==VAR_UNKNOWN))
            {
                return VAR_NUMERIC;
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_POWER:
            if((l_type==VAR_NUMERIC || l_type==VAR_UNKNOWN) &&
                (r_type==VAR_NUMERIC || r_type==VAR_UNKNOWN))
            {
                return VAR_NUMERIC;
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_EQ:
            if(l_type==VAR_UNDEFINED || r_type==VAR_UNDEFINED)
            {
                return VAR_UNDEFINED;
            }
            else
            {
                return VAR_BOOL;
            }
            break;
        case T_NE:
            if(l_type==VAR_UNDEFINED || r_type==VAR_UNDEFINED)
            {
                return VAR_UNDEFINED;
            }
            else
            {
                return VAR_BOOL;
            }
            break;
        case T_LT: /* intended falltrough */
        case T_GT:
        case T_LE:
        case T_GE:
            if((l_type==VAR_NUMERIC || l_type==VAR_UNKNOWN) &&
                    (r_type==VAR_NUMERIC || r_type==VAR_UNKNOWN))
            {
                return VAR_BOOL;
            }
            else if((l_type==VAR_STRING || l_type==VAR_UNKNOWN) &&
                    (r_type==VAR_STRING || r_type==VAR_UNKNOWN))
            {
                return VAR_BOOL;
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_AND: /* intended fallthrough */
        case T_OR:
        case T_NOT:
            return VAR_BOOL;
            break;
        case T_IN: /* intended fallthrough */
        case T_NOTIN:
            if(l_type!=VAR_UNDEFINED &&
                (r_type==VAR_ARRAY || r_type==VAR_STRING || r_type==VAR_UNKNOWN)) 
            {
                return VAR_BOOL;
            }
            else
            {
                return VAR_UNDEFINED;
            }
            break;
        case T_FUNCCALL:
            while(param != NULL)
            {
                if(typeOfExpression(param)==VAR_UNDEFINED)
                {
                    return VAR_UNDEFINED;
                }
                param=param->next;
            }
            return VAR_UNKNOWN;
            break;
        case T_INTFUNC:
            switch(expression->data.index)
            {
                case FUNC_PRINT:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_NIL;
                    break;
                case FUNC_INPUT:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_STRING;
                    break;
                case FUNC_NUMERIC:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_NUMERIC;
                    break;
                case FUNC_TYPEOF:
                    while(param != NULL)
                    {
                        self = typeOfExpression(param);
                        if(param->type == T_LITERAL && param->data.value.type == VAR_FUNCNAME)
                        {
                            param = param->next;
                        }
                        else if(self == VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        else
                        {
                            param = param->next;
                        }
                    }
                    return VAR_NUMERIC;
                    break;
                case FUNC_LEN:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_NUMERIC;
                    break;
                case FUNC_FIND:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_NUMERIC;
                    break;
                case FUNC_SORT:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_STRING;
                    break;
                case FUNC_ARRAYINIT:
                    while(param != NULL)
                    {
                        self=typeOfExpression(param);
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_ARRAY;
                    break;
                case FUNC_SUBVAR:
                    self=typeOfExpression(param);
                    if(self!=VAR_ARRAY && self!=VAR_STRING && self!=VAR_UNKNOWN)
                    {
                        return VAR_UNDEFINED;
                    }
                    param=param->next;
                    self=typeOfExpression(param);
                    if(self!=VAR_NUMERIC && self!=VAR_UNKNOWN)
                    {
                        return VAR_UNDEFINED;
                    }
                    param=param->next;
                    if(param==NULL)
                    {
                        return VAR_UNKNOWN;
                    }
                    self=typeOfExpression(param);
                    if(self!=VAR_NUMERIC && self!=VAR_UNKNOWN)
                    {
                        return VAR_UNDEFINED;
                    }

                    while(param != NULL)
                    {
                        if(self==VAR_UNDEFINED)
                        {
                            return VAR_UNDEFINED;
                        }
                        param=param->next;
                    }
                    return VAR_UNKNOWN; /* array or string */
                    break;
                case FUNC_ACCESS:
                    self=typeOfExpression(param);
                    if(self!=VAR_ARRAY && self!=VAR_STRING && self!=VAR_UNKNOWN)
                    {
                        return VAR_UNDEFINED;
                    }
                    self=typeOfExpression(param);
                    param=param->next;
                    if(self!=VAR_NUMERIC && self!=VAR_UNKNOWN)
                    {
                        return VAR_UNDEFINED;
                    }
                    return VAR_UNKNOWN; /* anything */
                    break;
                default:
                    fprintf(stderr,"FUNC_NOT_BUILTIN survived :-(\n");
                    exit(ERR_42);
                    break;
            }
            break;
        case T_UMINUS:
            if(l_type==VAR_NUMERIC || l_type==VAR_UNKNOWN)
            {
                return VAR_NUMERIC;
            }
            break;
     }
     return VAR_UNDEFINED;
}

/** Staticly checks types in the expression
 * 
 * @param line number of line, where the expression was written
 * @param expression pointer to the expression tree to check
 * @returns boolean whether the expression was found valid
 */ 
bool expressionValid(tExpr *expression, unsigned line)
{
    if(expression == NULL)
    {
        exit(ERR_42);
    }

    if(typeOfExpression(expression)==VAR_UNDEFINED)
    {
        fprintf(stderr,"Expression at line %u contains type invalidity\n",line);
        return false;
    }

    return true;
}
