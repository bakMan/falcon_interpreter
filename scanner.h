/**
 * @file scanner.h
 * @brief Interface of module for scanner implementation.
 * @author Dagmar Prokopova (xproko26)
 */

#ifndef _SCANNER_H_INCLUDED_
#define _SCANNER_H_INCLUDED_

#define KEY_WORDS 16
#define RES_WORDS 9


/* all possible types of token, order is not important here */
typedef enum TokenType {
    TUndefined,                 //0 undefined type
    TInvalid,                   //1 invalid type
    TInvalidNumInt,             //2 invalid type, set as integer in the srcfile
    TEOL,                       //3 End of Line
    TEOF,                       //4 End of File	
    TIdentifier,                //5 Identifier
    TLiteralNum,                //6 Numeric literal
    TLiteralStr,                //7 String literal
    TAssignment,                //8 =
    TLeftParenthesis,           //9 (
    TRightParenthesis,          //10 )
    TLeftBracketSquare,         //11 [
    TRightBracketSquare,        //12 ]
    TColon,                     //13 :
    TComma,                     //14 ,
    TOpPlus,                    //15 +
    TOpMinus,                   //16 -
    TOpUnMinus,                 //17 - (unary)
    TOpMultiplicator,           //18 *
    TOpDivider,                 //19 /
    TOpPower,                   //20 **
    TOpEqual,                   //21 ==
    TOpNotEqual,                //22 !=
    TOpLess,                    //23 <
    TOpGreater,                 //24 >
    TOpLessEqual,               //25 <=
    TOpGreaterEqual,            //26 >=
    TOpAt,                      //27 @
    TWrapString,                //28 Wrapped string following after @
    TWrapIdent,                 //29 Wrapped identifier preceded by $
    TReserveWord,               //30 One of Reserved Word
    TKW_and,                    //31 Key Word and
    TKW_elif,                   //32 Key Word elif
    TKW_else,                   //33 Key Word else
    TKW_end,                    //34 Key Word end
    TKW_false,                  //35 Key Word false
    TKW_for,                    //36 Key Word for
    TKW_function,               //37 Key Word function
    TKW_if,                     //38 Key Word if
    TKW_in,                     //39 Key Word in
    TKW_loop,                   //40 Key Word loop
    TKW_nil,                    //41 Key Word nil
    TKW_not,                    //42 Key Word not
    TKW_notin,                  //43 Key Word notin
    TKW_or,                     //44 Key Word or
    TKW_return,                 //45 Key Word return
    TKW_true,                   //46 Key Word true
    TKW_while,                  //47 Key Word while
    } TTokenType;


/* optional storage for data:
 * TLiteralNum - num
 * TInvalidNumInt - num
 * TLiteralStr - ptr
 * TIdentifier - ptr
 * TWrapString - ptr
 * TWrapIdent - ptr
 * TReserveWord - ptr
 * TKW_* - ptr
 */
typedef union tokenvalue {
    double num;
    char *ptr;
    } TValue;


/* token - send by scanner to syntax analyser */
/* --type - one of enum TokenType */
/* --value - double if numeric literal, string if identifier/stirng literal */
/* --length - count of chars of str.literal/identifier/keyword/... without '\0' */
/* --line - line of source file where token starts */
/* --col - column of source file where token starts */
typedef struct token {
    TTokenType type;
    TValue value;
    unsigned long length;
    unsigned int line;
    unsigned int col;
    } TToken;


/** Analyses the source file and returns one token. 
 *
 * Pointer to a source file is a global variable (see global.h), therefore no input
 * parameters are needed. Function is implemented as the finite state machine.
 * It returns token - structure containing token type, position in the source file
 * (line and col). If toknen type is Numeric Literal it contains also the value (double),
 * for Identifier|Keyword|Resword|StringLiteral it contains pointer to buffer where the
 * string is stored. When invalid token is found, it eats the rest of the line till the EOL
 * is found.
 *
 * @return structure containing token.type, [value (num or string)], position
 */
TToken getToken();

#endif // _SCANNER_H_INCLUDED_

