/**
 * @file      memory_interpret.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file for INTERPRET module of memory management for IFJ12
 *            interpret.
 *
 * @detailed  This header file contains specific interface for INTERPRET only.
 *            Interface common for every module is defined in memory.h file.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY_INTERPRET.H ]**************************************** *
 * ************************************************************************** */

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_INTERPRET_H
#define MEMORY_INTERPRET_H


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include "memory.h"                           /* Common interface. */


/* ************************************************************************** *
 ~ ~~~[ MODULE FUNCTIONAL PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

void *ifj_realloc(void *ptr, size_t size, TE_alloc_type type);

long int num_stack_pop(void);
bool NS_is_empty(void);
void func_call(void);
void *func_ret(void *src, size_t size);
void expr_fin(void);
void func_end(void);

/* ************************************************************************** *
 ~ ~~~[ MACROS TO SUBSTITUTE MISSING FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */
#define num_stack_push(x) do{*((long *)ifj_malloc(sizeof(long),NUM_STACK)) = x;}while(0)

/* ************************************************************************** *
 * ***[ END OF MEMORY_INTERPRET.H ]****************************************** *
 * ************************************************************************** */

#endif

