/**
 * @file      memory_scanner.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file for SCANNER module of memory management for IFJ12
 *            interpret.
 *
 * @detailed  This header file contains specific interface for SCANNER only.
 *            Interface common for every module is defined in memory.h file.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY_SCANNER.H ]****************************************** *
 * ************************************************************************** */

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_SCANNER_H
#define MEMORY_SCANNER_H


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include "memory.h"                           /* Common interface. */


/* ************************************************************************** *
 ~ ~~~[ CONSTANTS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#define BUFFER_LENGTH SCANNER_BUFFER_SIZE     /* As defined in memory.h */


/* ************************************************************************** *
 ~ ~~~[ MODULE FUNCTIONAL PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

void *ifj_realloc(void *ptr, size_t size, TE_alloc_type type);


/* ************************************************************************** *
 * ***[ END OF MEMORY_SCANNER.H ]******************************************** *
 * ************************************************************************** */

#endif

