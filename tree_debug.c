/**
 * @file   tree_debug.h
 * @brief  Functions for graphical syntactic tree dumping
 * @author Stanislav Smatana (xsmata01) 
 *
 * @todo: Pozor na stringy s newlinom vo variabloch !! 
 */
#include "structures.h"
#include "tree_debug.h"
#include <stdlib.h>

#define CMD_COL   "#CCD9FF"
#define EXPR_COL  "#FFE18F"
#define VAR_COL   "#FFD8CC"
#define FUNC_COL  "#D8CCFF"

#define CMD_SHAPE    "record"
#define EXPR_SHAPE   "ellipse"
#define VAR_SHAPE    "Mrecord"
#define FUNC_SHAPE    "Mrecord"

#define PREFIX    "\"%p\" "
#define POSTFIX   "\"];\n"

#define CMD_PREFIX  PREFIX "[shape=" CMD_SHAPE ",style=filled, color =\"" CMD_COL "\", label=\""
#define VAR_PREFIX  PREFIX "[shape=" VAR_SHAPE ",style=filled, color =\"" VAR_COL "\", label=\""
#define EXPR_PREFIX PREFIX "[shape=" EXPR_SHAPE ",style=filled, color =\"" EXPR_COL  "\", label=\""
#define FUNC_PREFIX PREFIX "[shape=" FUNC_SHAPE ",style=filled, color =\"" FUNC_COL  "\", label=\""

const char *cmd_patterns[]=
{
   [COMM_ASSIGNMENT]    = CMD_PREFIX "{<f0> ASSIGMENT | <expr>Index %d =| <next> Next}"  POSTFIX,
   [COMM_IFELS]         = CMD_PREFIX "{<f0> IF | <cond>Cond | <next> Then |<else> Else}" POSTFIX,
   [COMM_RETURN]        = CMD_PREFIX "{<f0> RETURN | <expr>Expression }" POSTFIX,
   [COMM_PASS]          = CMD_PREFIX "{<f0> PASS | <next> Next}" POSTFIX,
};


const char *expr_patterns[]=
{
   //T_LITERAL a T_VARIABLE parsuje sa specialne
   [T_PLUS]       = "+",
   [T_MINUS]      = "-",
   [T_ASTERISK]   = "*",
   [T_SLASH]      = "/",
   [T_POWER]      = "**",
   [T_EQ]         = "==",
   [T_NE]         = "!=",
   [T_LT]         = "<",   // less than
   [T_GT]         = ">",   // greater than
   [T_LE]         = "<=",   // less or equal than
   [T_GE]         = ">=",   // greater or equal than
   [T_AND]        = "&",
   [T_OR]         = "|",
   [T_NOT]        = "!",
   [T_IN]         = "in",
   [T_NOTIN]      = "not in",
   [T_FUNCCALL]   = FUNC_PREFIX "{<f0> FUNCTION|<param> Params: %u|<cmd> Commands}" POSTFIX,
   [T_INTFUNC]    = FUNC_PREFIX "{<f0>%s|<param> Params}" POSTFIX,
};

const char *internal_funcs[]=
{
   [FUNC_PRINT] = "print()",
   [FUNC_INPUT] = "input()",
};

const char *var_patterns[]={
   [VAR_UNDEFINED]   = VAR_PREFIX "{UNDEFINED}" POSTFIX,
   [VAR_NIL]         = VAR_PREFIX "{NIL}" POSTFIX,
   [VAR_BOOL]        = VAR_PREFIX "{BOOL|%s}" POSTFIX,
   [VAR_NUMERIC]     = VAR_PREFIX "{NUMERIC|%f}" POSTFIX,
   // [VAR_STRING]      = VAR_PREFIX "{STRING|\\\"%s\\\"}" POSTFIX, //escapovanie - do zdrojaku pre dot treba \"blah\"
   [VAR_STRING]      = VAR_PREFIX "{STRING}" POSTFIX, //escapovanie - do zdrojaku pre dot treba \"blah\"
   [VAR_ARRAY]       = "",
};



/** Binary search tree node structure. */
typedef struct tptreenode tPtreeNode;
struct tptreenode
{
   // Searching key - string.
   void *key;
   // Pointers to left and right subtree
   tPtreeNode *lptr,*rptr;

} ;


tPtreeNode *global_tree;

tPtreeNode *ptreeCreateTree();
void ptreeDestroy(tPtreeNode *root_node);
bool ptreeInsert(tPtreeNode *tree_root, void *key);
bool ptreeContains(tPtreeNode *tree_root,void *key);

//print relation
//helper ktory printuje relaciu dvoch entit = sipka na grafe
static inline void printRel(FILE *fp, void *a, const char *field,  void *b)
{
   if(field != NULL)
      fprintf(fp,"\"%p\":%s -> \"%p\";\n",a,field,b);
   else
      fprintf(fp,"\"%p\" -> \"%p\";\n",a,b);
}

void dumpFunction(FILE *fp,tFunc *function);
void dumpVariable(FILE *fp, variable *var);
void dumpCommands(FILE *fp,tCommand *commands, tCommand *stopPtr);


//checkni ci sedi left a right child expressionu
void dumpExpression(FILE *fp,tExpr *expression)
{
   while(expression != NULL)
   {
      switch(expression->type)
      {
         case T_FUNCCALL:
           fprintf(fp, expr_patterns[T_FUNCCALL], (void *) expression,expression->data.function->paramnumber);
           if(expression->params != NULL)
           {
              printRel(fp,expression,"param",expression->params);
              dumpExpression(fp,expression->params);
           }
           if(!ptreeContains(global_tree,expression->data.function))
           {
              ptreeInsert(global_tree,expression);
              printRel(fp,expression,"cmd",expression->data.function->commands);

              if(expression->data.function->commands != NULL)
                 dumpCommands(fp, expression->data.function->commands, NULL);
           }
           break;

         case T_INTFUNC:
           fprintf(fp,expr_patterns[T_INTFUNC],expression,internal_funcs[expression->data.index]);
           if(expression->params != NULL)
           {
              printRel(fp,expression,"param",expression->params);
              dumpExpression(fp,expression->params);
           }
           
           break;

         case T_LITERAL:
         case T_VARIABLE:
            fprintf(fp, EXPR_PREFIX "VARIABLE" POSTFIX,(void *) expression);
            printRel(fp,expression,NULL,&(expression->data.value));
            dumpVariable(fp,&(expression->data.value));
            break;

         default:
            fprintf(fp, EXPR_PREFIX "%s" POSTFIX,(void *) expression,expr_patterns[expression->type]);
            printRel(fp,expression,NULL,expression->data.children[0]);
            printRel(fp,expression,NULL,expression->data.children[1]);
            dumpExpression(fp,expression->data.children[0]);
            dumpExpression(fp,expression->data.children[1]);
            break;
      }

      if(expression->next != NULL)
         printRel(fp,expression,NULL,expression->next);

      expression = expression->next;
   }
}

//funkcia pre dumpovanie commandlistov
//zarazka sluzi pri cykloch na zastavenie vykreslovania ked sa 
//vratime k testovacej podmienke - param NULL znamena ze koncime
//dumpovanie len pri NULL
void dumpCommands(FILE *fp,tCommand *commands, tCommand *stopPtr)
{
   // tu dojde k ukonceniu ak bola nastavena zarazka stopPtr
   while(commands != NULL && commands != stopPtr)
   {
      switch(commands->type)
      {
         case COMM_ASSIGNMENT:
            fprintf(fp,cmd_patterns[COMM_ASSIGNMENT],commands,commands->index);
            printRel(fp,commands,"expr",commands->expression);
            dumpExpression(fp,commands->expression);
            break;

         case COMM_RETURN:
            fprintf(fp,cmd_patterns[COMM_RETURN],commands);
            printRel(fp,commands,"expr",commands->expression);
            dumpExpression(fp,commands->expression);
            break;

         case COMM_PASS: // ify sa zbiehaju na PASS ak uz je v ptree -> koniec ina vetva tam uz bola
            if(ptreeContains(global_tree,commands))
               return;

            ptreeInsert(global_tree,commands);
            fprintf(fp,cmd_patterns[COMM_PASS],commands);
            break;
            
         case COMM_IFELS:
            fprintf(fp,cmd_patterns[COMM_IFELS],commands);
            printRel(fp,commands,"cond",commands->expression);
            printRel(fp,commands,"else",commands->alternative);
            dumpExpression(fp,commands->expression);
            
            dumpCommands(fp, commands->next, commands); //vykreslenie else, tento if je zarazka -> kvoli cyklom
          

      }

      //stupid 
      if(commands->next != NULL )
         printRel(fp,commands,"next",commands->next);
      
      // ak ide o cyklus musime ist dalej po alternativnej vetve 
      if( commands->type == COMM_IFELS)
         commands = commands->alternative;
      else
         commands = commands->next;
   }

}


//helper pre dumpovanie variablov
//
//@todo: Dorob arrays
void dumpVariable(FILE *fp, variable *var)
{
   switch(var->type)
   {
      case VAR_UNDEFINED:
      case VAR_NIL:
         fprintf(fp,var_patterns[var->type],var);
         break;

      case VAR_BOOL:
         fprintf(fp,var_patterns[VAR_BOOL],var, var->value.boolean ? "TRUE" : "FALSE");
         break;

      case VAR_NUMERIC:
         fprintf(fp,var_patterns[VAR_NUMERIC],var, var->value.numeric);
         break;

      case VAR_STRING:
         fprintf(fp,var_patterns[VAR_STRING],var);
         break;

      default:
         break;
   }
}

//helper pre dumpovanie funkcii
void dumpFunction(FILE *fp,tFunc *function)
{
   fprintf(fp,expr_patterns[T_FUNCCALL],function,function->paramnumber);
   
   //ak nie su commandy null tak ich parsujeme
   if(function->commands != NULL)
   {
      printRel(fp,function,"cmd",function->commands);
      dumpCommands(fp, function->commands, NULL);
   }
}

//modifikacia binarneho stromu pre odstranenie duplicit a rekurzivneho
//zacyklenia
tPtreeNode *ptreeCreateTree()
{
   tPtreeNode *root_node;

   if( (root_node  = (tPtreeNode *) malloc(sizeof(tPtreeNode))) == NULL)
      return NULL; 

   root_node->lptr=NULL;
   root_node->rptr=NULL;
   root_node->key=NULL;

   return root_node;
}

void ptreeDestroy(tPtreeNode *root_node)
{
   tPtreeNode *lptr,*rptr;
   lptr = root_node->lptr;
   rptr = root_node->rptr;

   //poradie je zvolene aby nevznikali mrtve ramena rekurzie 
   free(root_node);
   if (lptr != NULL) 
      ptreeDestroy(lptr);

   if (rptr != NULL) 
      ptreeDestroy(rptr);

}


bool ptreeInsert(tPtreeNode *tree_root, void *key)
{
   if(tree_root->key == NULL)
   {
      tree_root->key=key;
   }
   else
   {
      // dostaneme sa na koniec
      // for posuva hodnotu ukazatela tmp vzdy bud na ADRESU ukazatela
      // lptr alebo rptr podla vysledku strcmp
      // cyklus sa ukonci pokial je hodnota na *tmp NULL
      // tmp vtedy obsahuje ADRESU ukazatela ktory bude ukazovat na novy prvok
      tPtreeNode **tmp;
      for(tmp = &tree_root; *tmp != NULL; tmp = (key < ((*tmp)->key) ? &((*tmp)->lptr) : &((*tmp)->rptr)) ); 
      
      if( (*tmp = (tPtreeNode *) malloc(sizeof(tPtreeNode))) == NULL)
         return false;
      else
      {
        (*tmp)->key=key;
        (*tmp)->lptr=NULL;
        (*tmp)->rptr=NULL;
      }


   }

   return true;
}

bool ptreeContains(tPtreeNode *tree_root,void *key)
{
   // prazdny strom
   if (tree_root->key == NULL)
      return false;

   //cyklus prehladava strom pokial sa hladany kluc nerovna sucasnemu
   //v kazdom kroku posunie podla hodnoty porovnania tree_root bud na lptr 
   //alebo rptr -> ak po posunuti dostaneme NULL znamena to ze skoncil
   //a element nenasiel
   while( key != tree_root->key )
      if ( (tree_root = key < tree_root->key ? tree_root->lptr : tree_root->rptr) == NULL)
         return false;

   return true;
}



bool dumpProgramGraph(const char* output_file,void *root_function)
{
   tFunc *root = (tFunc *) root_function;
   
   FILE *fp;

   if(output_file == NULL)
      fp=stdout;
   else
   {
      if( (fp = fopen(output_file,"w")) == NULL)
         return false;
   }

   //vytvorenie globalneho stromu pointrov
   global_tree = ptreeCreateTree();

   //zapiseme hlavicku suboru
   fprintf(fp,"strict digraph G{\n");

   //telo bude tree dump
   dumpFunction(fp,root);

   //znicenie stromu pointrov
   ptreeDestroy(global_tree);

   fprintf(fp,"}");
   fclose(fp);

   return true;
}
