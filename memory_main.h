/**
 * @file      memory_main.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file for MAIN module of memory management for IFJ12
 *            interpreter.
 *
 * @detailed  This header file contains specific interface for MAIN only.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY_MAIN.H ]********************************************* *
 * ************************************************************************** */

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_MAIN_H
#define MEMORY_MAIN_H


/* ************************************************************************** *
 ~ ~~~[ MODULE DATA TYPES DECLARATIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/**
 * Enumerate representing current run stage of IFJ12 interpreter which is about
 * to begin.
 */
typedef enum run_stage {
  PARSER,                               /* Parser & scanner stage. */

#ifdef OPTIMIZER_ON
  OPTIMIZER,                            /* Optimizer stage. */
#endif

  INTERPRET,                            /* Execution of the code. */
} TE_stage;


/* ************************************************************************** *
 ~ ~~~[ MODULE FUNCTIONAL PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

void memory_setup(TE_stage stage);


/* ************************************************************************** *
 * ***[ END OF MEMORY_MAIN.H ]*********************************************** *
 * ************************************************************************** */

#endif

