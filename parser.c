/**
 * @file parser.c
 * @brief Syntactic parser, the very heart of the compiler part.
 * @author Karel Benes
 * @version 0.many
 */

#include <stdlib.h>
#include <string.h>

#include "memory_parser.h"
#include "parser.h"
#include "type_checking.h"

/** points to the last synthetised command in the "main()"*/
tCommand *last_main_command=NULL;

/** points to the vartable of the "main()" */
tBSTNode *main_vartable = NULL;

/** points to the function table */
tBSTNode *functable = NULL;

/** number of defined variables in the "main()" */
unsigned main_tsp = 0;

/** auxiliary boolean for buffered reading of tokens. Becomes true when
    a token is "returned" */
bool buffer_valid=false;

/* declaration needed to keep this a valid C program */
tCommand *commandList(tCommand *predecessor, tBSTNode *vartable, unsigned *vartable_tsp);
tCommand *command(tCommand *,tBSTNode *, unsigned *);
tCommand *ifElse(tCommand *predecessor, tBSTNode *vartable, unsigned *vartable_tsp);
intFunction builtinFunction(char *);

/** Types of error occuring during the syntax analysis.
 *
 * Is used mostly as super-format string for reporting errors.
 */
typedef enum{
    UNEXPECTED,         /**< when there is no LL rule for the token */
    ILL_EXPR_TOKEN,     /**< token unacceptable in expression such as 'function' etc. */
    PREC_TABLE_ERROR,   /**< when precedence table claims it is inacceptable */
    RULE_UNAPPLIABLE,   /**< when the precedence parsing pushdown state
        rules out rule apliccation :-) */
    UNDEFINED_FUNCTION, /**< function call to an undefined function */
    BUILTIN_FUNC_VAR,   /**< trying to use builtin function as a variable */
    TWO_SAME_FORMAL_PARAMETERS,   /**< i.e. function foo(x,x) */
    FV_NAME_CONFLICT,    /**< variable of same name as (user) function */
    UNDEFINED_SYMBOL,   /**< undefined symbol. Wow.*/
    OTHER               /**<  used by LL when more options would be acceptable */
}TParseError;


/** Array of strings corresponding to token types.
 *
 * If you need to print token's type, this is what you are looking for.
 */
const char *token_text[] = {
    [TUndefined] = "'Undefined'",
    [TInvalid] = "'Invalid type'",
    [TInvalidNumInt] = "'Invalid integer'",
    [TEOL] = "'EOL'",
    [TEOF] = "'EOF'",
    [TIdentifier] = "'Identifier'",
    [TLiteralNum] = "'Numeric literal'",
    [TLiteralStr] = "'String literal'",
    [TAssignment] = "'='",
    [TLeftParenthesis] = "'('",
    [TRightParenthesis] = "')'",
    [TLeftBracketSquare] = "'['",
    [TRightBracketSquare] = "']'",
    [TColon] = "':'",
    [TComma] = "','",
    [TOpPlus] = "'+'",
    [TOpMinus] = "'-'",
    [TOpUnMinus] = "'unary minus'",
    [TOpMultiplicator] = "'*'",
    [TOpDivider] = "'/'",
    [TOpPower] = "'**'",
    [TOpEqual] = "'=='",
    [TOpNotEqual] = "'!='",
    [TOpLess] = "'<'",
    [TOpGreater] = "'>'",
    [TOpLessEqual] = "'<='",
    [TOpGreaterEqual] = "'>='",
    [TOpAt] = "'@'",
    [TWrapString] = "'wrapped string'",
    [TWrapIdent] = "'wrapped identifier'",
    [TReserveWord] = "'reserved word'",
    [TKW_and] = "'and'",
    [TKW_else] = "'else'",
    [TKW_end] = "'end'",
    [TKW_false] = "'false'",
    [TKW_function] = "'function'",
    [TKW_if] = "'if'",
    [TKW_in] = "'in'",
    [TKW_loop] = "'loop'",
    [TKW_nil] = "'nil'",
    [TKW_not] = "'not'",
    [TKW_notin] = "'notin'",
    [TKW_or] = "'or'",
    [TKW_return] = "'return'",
    [TKW_true] = "'true'",
    [TKW_while] = "'while'"
};

/** Function call list implementation.
 *
 * This is used to collect function calls thorough the script (except calling
 * the builtin functionc). This list is built as the parsing procceeds and is
 * destroyed at once while checking its content.
 */
typedef struct t_function_call TFunctionCall;

struct t_function_call{
    TFunctionCall *next; /** List creating pointer */
    TToken generator; /** The id in "id(param1, param2, ...)" */
    tExpr *expr; /** Pointer to the FUNC_CALL expression node. */
};
/** We need tail for queue-like adding and head for going through the list
 * while checking correctness of its contents.
 */
TFunctionCall *fcl_head=NULL,*fcl_tail=NULL;

/** Expression list implementation.
 *
 * This is used to collect expression thorough the script, so they can be later
 * examined for static type invalidities.
 */
typedef struct t_expression_tree TExpressionTree;

struct t_expression_tree{
    TExpressionTree *next; /**< list creating pointer*/
    tExpr *expr; /**< pointer to the expression to be checked */
    unsigned line; /**< line where the expression was written*/
};
/** We need the list to behave queue-like, thus we declare both head and tail
 * pointers.
 */
TExpressionTree *etl_head=NULL,*etl_tail=NULL;

/** Variable names list implementation.
 *
 * This is used to collect variable names thorough the script, so they can be
 * later examined for conflicts with user defined functions.
 */
typedef struct t_var_name TVarName;

struct t_var_name{
    TVarName *next; /**< list creating pointer*/
    TToken generator; /**< token, where the variable was declared */
};

/** We need this list to behave queue-like, thus we declare both head and taik
 * pointers.
 */
TVarName *vnl_head=NULL, *vnl_tail=NULL;

/** Undefined symbols list inplementation.
 *
 * It is possible that undefined symbol was a function name so we need to provide
 * this wonderful feature to the user.
 *
 * Since we need sometimes to remove the last item, we have this be a double
 * linked list.
 */
typedef struct t_undef_symbol TUndefSymbol;

struct t_undef_symbol{
    TUndefSymbol *next,*prev; /**< list creating pointer*/
    TToken generator; /**< token representing the undefined symbol */
};

/** We need this list to behave queue-like, thus we declare both head and taik
 * pointers.
 */
TUndefSymbol *usl_head=NULL,*usl_tail=NULL;


/** Head & tail pointer to loop list used by optimizer.
 */
TLoop *sll_head,*sll_tail;

/** Symbolic constants for precedence rules.
 * @warning It's essential, that ERROR is equal to 0, because I'm
 * using it for initializing only 'useful' parts of the table.
 */
typedef enum{
    ERROR,  //< Unacceptable combination (uppermost terminal, read token)
    REDUCE, //< Apply a reduction rule
    SHIFT,  //< Push the read token upon the stack
    SUCCESS, //< Reaching (BOTTOM,EOL) indices successful expression parsing
}TPrecedenceRule;

/** Precedence table.
 *
 * This is a really big piece of the code. It is organized in the [pushdown
 * topmost terminal, input token] manner. SHIFT rule stands for simply pushing
 * the symbol upon the pushdown, while REDUCE rule rewrites 1 to 4 topmost
 * items to a new nonterminal. Reaching the [BOTTOM,EOL] or [BOTTOM,assignment]
 * is the only successful way out of processing the the expression.
 *
 * The content itself has been moved into the "prec_rules.arr" file.
 */
/* TUndefined serves as pushdown bottom alias */
const TPrecedenceRule precedence_table[TKW_while][TKW_while] =
{
#include "prec_rules.arr"
};

/** General error printing function for the syntax parser.
 *
 * @param err_type specifies type of the error
 * @param token declares where the error was encountered
 * @param additional carries some additional info, such as what was expected
 * instead
 *
 */
void parserError(TParseError err_type, TToken token, TTokenType additional)
{
    if(err_type == UNEXPECTED)
    {
        fprintf(stderr,"%u:%u unexpected %s, expected %s\n",\
         token.line,token.col,token_text[token.type],token_text[additional]);
    }
    else if(err_type == ILL_EXPR_TOKEN)
    {
        fprintf(stderr,"%u:%u %s illegal in an expression\n",\
         token.line,token.col,token_text[token.type]);
    }
    else if(err_type == PREC_TABLE_ERROR)
    {
        fprintf(stderr,"%u:%u precedence table claims [%s,%s] is illegal\n",\
         token.line,token.col,token_text[additional],token_text[token.type]);
    }
    else if(err_type == RULE_UNAPPLIABLE)
    {
        fprintf(stderr,"%u:%u reduce impossible while reading %s\n",\
         token.line,token.col,token_text[token.type]);
    }
    else if(err_type == UNDEFINED_FUNCTION)
    {
        fprintf(stderr,"%u:%u undefined function '%s'\n",\
         token.line,token.col,token.value.ptr);
    }
    else if(err_type == BUILTIN_FUNC_VAR)
    {
        fprintf(stderr,"%u:%u builtin function name '%s' cannot be used as a variable\n",\
         token.line,token.col,token.value.ptr);
    }
    else if(err_type == TWO_SAME_FORMAL_PARAMETERS)
    {
        fprintf(stderr,"%u:%u second formal parameter of the name '%s'\n",\
         token.line,token.col,token.value.ptr);
    }
    else if(err_type == FV_NAME_CONFLICT)
    {
        fprintf(stderr,"%u:%u '%s' is a function, cannot be defined as variable\n",\
         token.line,token.col,token.value.ptr);
    }
    else if(err_type == UNDEFINED_SYMBOL)
    {
        fprintf(stderr,"%u:%u '%s' undefined\n",\
         token.line,token.col,token.value.ptr);
    }
    else if(err_type == OTHER)
    {
        fprintf(stderr,"%u:%u unspecified error encountered at %s\n",\
            token.line,token.col,token_text[token.type]);
    }
    else
    {
        fprintf(stderr,"this never happens\n");
        exit(ERR_42);
    }

}

/** Buffered token reading.
 *
 * Together with returnToken() provides one-token buffer.
 *
 * @returns token (either from buffer or from the source file)
 */
TToken bufferedToken(void)
{
    static TToken buffer;

    if(buffer_valid)
    {
        /* if we give out the token, it is no more on the input */
        buffer_valid=false;
        return buffer;
    }
    else
    {
        buffer = getToken();
        return buffer;
    }
}

/** Revalidates buffer content.
 *
 * Fuctionally, this is equal to 'pushing' the token back in the input stream.
 */
void returnToken(void)
{
    buffer_valid = true;
}


/** Pushes an item upon the stack.
 *
 * @param stack pointer to pointer to the topmost item of the stack
 * @param item value, which is to be stored upon the stack
 */
void push(TPushdownItem **stack, TPushdownItem item)
{
    TPushdownItem *ptr;
    ptr = ifj_malloc(sizeof(TPushdownItem),PARSER_STACK);

    *ptr = item;
    ptr->next = *stack;
    *stack = ptr;
}

/** Collects function call.
 *
 * Builds a list from these (therefore calls malloc(), and therefore
 * may never return).
 * @param func_call the function call to be remembered
 */
void addFunctionCall(TFunctionCall func_call)
{
    TFunctionCall *item = ifj_malloc(sizeof(TFunctionCall),PARSER_PERS);
    *item = func_call;
    item->next = NULL;

    /* As prof. Honzik keeps repeating, we treat the beginning seperate */
    if(fcl_head==NULL)
    {
        fcl_head = item;
        fcl_tail = item;
    }
    else
    {
        fcl_tail->next = item;
        fcl_tail=item;
    }
}

/** Goes through all the function calls in the script and checks their validity.
 *
 * This function needs to be called at the very end of analysis & synthesis.
 * Task of this function is to 1] check, whether there is a function of
 * the required name and 2] if so, it substitutes the symbolic information (name)
 * in the function call expression with a concrete address of the function
 * synthetised prototype.
 *
 * In addition frees the function call list and (therefore) doesn't stop at first
 * encountered error.
 *
 * @returns boolean claiming wheter all the function calls were OK
 */
bool checkFunctionCalls(void)
{
    bool retval=true;

    while(fcl_head != NULL)
    {
        void **ptr = bstLookUp(functable,fcl_head->generator.value.ptr);
        if(ptr == NULL)
        {
            parserError(UNDEFINED_FUNCTION,fcl_head->generator,TUndefined);
            retval = false;
        }
        else
        {
            fcl_head->expr->data.function=*ptr;
        }
        fcl_head = fcl_head->next;
    }

    return retval;
}

/** Collects variable names.
 *
 * Builds a list from these (therefore calls malloc(), and therefore
 * may never return).
 * @param generator the token at which the variable was defined
 */
void addVarName(TToken generator)
{
    TVarName *item = ifj_malloc(sizeof(TVarName),PARSER_PERS);
    item->generator = generator;
    item->next = NULL;

    /* As prof. Honzik keeps repeating, we treat the beginning seperate */
    if(vnl_head==NULL)
    {
        vnl_head = item;
        vnl_tail = item;
    }
    else
    {
        vnl_tail->next = item;
        vnl_tail=item;
    }
}

/** Goes through all the variable names in the script and checks them for conflict.
 *
 * This function needs to be called at the very end of analysis & synthesis.
 * Every variable name in the list is checked for conflict with user defined
 * functions.
 *
 * In addition frees the variable name list and (therefore) doesn't stop at first
 * encountered error.
 *
 * @returns boolean claiming wheter all the variable names were OK
 */
bool checkVariableNames(void)
{
    bool retval=true;

    while(vnl_head != NULL)
    {
//printf("checking %s\n",vnl_head->generator.value.ptr);
        void **ptr = bstLookUp(functable,vnl_head->generator.value.ptr);
        intFunction int_function = builtinFunction(vnl_head->generator.value.ptr);
        if(ptr != NULL || int_function != FUNC_NOT_BUILTIN)
        {
            parserError(FV_NAME_CONFLICT,vnl_head->generator,TUndefined);
            retval = false;
        }

        vnl_head = vnl_head->next;
    }

    return retval;
}

/** Collects undefined symbols.
 *
 * Builds a list from these (therefore calls malloc(), and therefore
 * may never return).
 * @param generator the token at which the undefined symbol was encountered
 */
void addUndefSymbol(TToken generator)
{
    TUndefSymbol *item = ifj_malloc(sizeof(TUndefSymbol),PARSER_PERS);
    item->generator = generator;
    item->next = NULL;

    /* As prof. Honzik keeps repeating, we treat the beginning seperate */
    if(usl_head==NULL)
    {
        item->prev = NULL;
        usl_head = item;
        usl_tail = item;
    }
    else
    {
        item->prev = usl_tail;
        usl_tail->next = item;
        usl_tail=item;
    }
}

/** Goes through all the undefined symbols in the script and checks them
 * for possible validity in sense of being a 'function name'.
 *
 * This function needs to be called at the very end of analysis & synthesis.
 * Every variable name in the list is checked for conflict with user defined
 * functions.
 *
 * @returns boolean claiming wheter all the variable names were OK
 */
bool checkUndefSymbols(void)
{
    bool retval=true;

    while(usl_head != NULL)
    {
        void **ptr = bstLookUp(functable,usl_head->generator.value.ptr);
        intFunction int_function = builtinFunction(usl_head->generator.value.ptr);
        if(ptr == NULL && int_function == FUNC_NOT_BUILTIN)
        {
            parserError(UNDEFINED_SYMBOL,usl_head->generator,TUndefined);
            retval = false;
        }

        usl_head = usl_head->next;
    }

    return retval;
}


/***********************************************************************************/
/** Collects expression trees.
 *
 * Builds a list from these (therefore calls malloc(), and therefore
 * may never return).
 * @param expr_tree the expression tree to be remembered
 */
void addExpressionTree(tExpr *expr_tree, unsigned line)
{
    TExpressionTree *item = ifj_malloc(sizeof(TExpressionTree),PARSER_PERS);
    item->expr = expr_tree;
    item->line = line;
    item->next = NULL;

    /* As prof. Honzik keeps repeating, we treat the beginning seperate */
    if(etl_head==NULL)
    {
        etl_head = item;
        etl_tail = item;
    }
    else
    {
        etl_tail->next = item;
        etl_tail=item;
    }
}

/** Goes through all the expression trees in the script and checks their validity.
 *
 * This function needs to be called at the very end of analysis & synthesis.
 * Task of this function is to have the expression checked for type incompatibility.
 *
 * In addition frees the function call list and (therefore) doesn't stop at first
 * encountered error.
 *
 * @returns boolean claiming wheter all the function calls were OK
 */
bool checkExpressionTrees(void)
{
    bool retval=true;

    while(etl_head != NULL)
    {
        if(!expressionValid(etl_head->expr,etl_head->line)) //!!!!!!!
        {
            retval = false;
        }

        etl_head = etl_head->next;
    }

    return retval;
}
/***********************************************************************************/

/** Prints a pushdown item in a human readable way.
 *
 * Is not likely to be used in the final program.
 *
 * @param output a FILE * pointer to output stream
 * @param item the pushdown item to be put down
 */
void printPushdownItem(FILE *output, TPushdownItem item)
{
    if(item.type == BOTTOM)
    {
        fprintf(output,"'BOTTOM'");
    }
    else if(item.type == NONTERMINAL)
    {
        fprintf(output,"'NONTERMINAL'");
    }
    else
    {
        fprintf(output,"%s",token_text[item.content.term.type]);
    }
}


/** Classifies token for needs of precedence parsing of expressions.
 *
 * So far only the ILLEGAL class is taken into account, but this is to be used
 * for final program clarity.
 *
 * @param token token to be classified
 * @returns token class
 */
TExprTermClass classifyToken(TToken token)
{
    switch(token.type)
    {
        case TOpPlus:
        case TOpMinus:
        case TOpUnMinus:
        case TOpMultiplicator:
        case TOpDivider:
        case TOpPower: return ARIT_BINOP;

        case TOpEqual:
        case TOpNotEqual:
        case TOpLess:
        case TOpGreater:
        case TOpLessEqual:
        case TOpGreaterEqual:
        case TKW_in:
        case TKW_notin: return REL_BINOP;

        case TKW_not:
        case TKW_or:
        case TKW_and: return LOG_BINOP;

        case TComma:
        case TOpAt:
        case TWrapString:
        case TWrapIdent:
        case TColon:
        case TAssignment:
        case TEOL: return MISC;

        case TIdentifier:
        case TKW_nil:
        case TKW_true:
        case TKW_false:
        case TLiteralNum:
        case TLiteralStr: return OPERAND;

        case TLeftParenthesis:
        case TRightParenthesis:
        case TLeftBracketSquare:
        case TRightBracketSquare: return PAR;

        default:
            return ILLEGAL;
    }
}

/** Table used to compare an identifier with a builtin funtion.
 *
 * Also used for printing the function called, thats why there is
 * FUNC_NOT_BUILTIN in the table.
 */
const char *builtin_func_ident[] =
{
    [FUNC_PRINT] = "print",
    [FUNC_INPUT] = "input",
    [FUNC_NUMERIC] = "numeric",
    [FUNC_TYPEOF] = "typeOf",
    [FUNC_LEN] = "len",
    [FUNC_FIND] = "find",
    [FUNC_SORT] = "sort",
    [FUNC_ARRAYINIT] = "", /* these functions are not directly callable */
    [FUNC_SUBVAR] = "",
    [FUNC_ACCESS] = "",
    [FUNC_NOT_BUILTIN] = "not a built-in funtion",
};


/** Decides wheter an identifier is a builtin function.
 *
 * @param identifier pointer to the string-to-be-examined
 * @returns index of the builtin function or a symbol for not a builtin function
 */
intFunction builtinFunction(char *identifier)
{
    intFunction i;
    for(i=0;i<FUNC_NOT_BUILTIN;i++)
    {
        if(strcmp(identifier,builtin_func_ident[i])==0)
        {
            break;
        }
    }

    return i;
}

bool checkFuncNames(void)
{
    bool retval = true;
    for(int i=0;i<FUNC_NOT_BUILTIN;i++)
    {
        if(bstLookUp(functable,builtin_func_ident[i])!=NULL)
        {
            retval = false;
            fprintf(stderr,"user definition of %s(). Forbidden.\n",\
                builtin_func_ident[i]);
        }
    }

    return retval;
}

/** Applies the REDUCE rule.
 *
 * This is where the expression tree synthesis is carried out.
 *
 * @param stack where the pointer to the pushdown's topmost item is
 * @param the actual input token
 * @param um_terminal the uppermost terminal found on the stack
 * @param new_node pointer to a space ready to hold a new expression node
 * @param vartable pointer to a vartable. Used to check variable definition.
 * @returns whether the reduction was succesfully applied
 */
bool applyReduce(TPushdownItem **stack,TToken token,\
                    TToken um_terminal, tExpr *new_node, tBSTNode *vartable)
{
    TPushdownItem SP0, SP1, SP2; // Stack point +0, +1 and +2 respectively
    TPushdownItem new_nonterminal; // What shall be pushed back on the stack

    new_nonterminal.type = NONTERMINAL;
    variable var; // for constant synthesis

    switch(um_terminal.type)
    {
        case TIdentifier:
            /* <Expr> -> id
             * For this case we only pop one item from the stack
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* we check if the variable is already defined */
            unsigned **index = (unsigned **) bstLookUp(vartable,\
                                        SP0.content.term.value.ptr);
            /* if it is not, we optimisticly assume it is valid function name
             * at a valid spot (both assumptions will be put to a test later)
             *
             * note that we do NOT care about the name of the function
             */
            if(index == NULL)
            {
                var=(variable){.type=VAR_FUNCNAME, .no_touch=true};
                new_node->data.value = var;
                new_node->type = T_LITERAL;
                addUndefSymbol(SP0.content.term);
            }
            /* if it is properly defined already, we set the expression
             * node aproperietly */
            else
            {
                new_node->type = T_VARIABLE;
                new_node->data.index = **index;
            }
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;

            /* and we push it onto the stack */
            push(stack,new_nonterminal);
            break;

        case TWrapString:
        /* strange extension <Expr> -> @"strageness itself", part 1 */
            SP0 = pop(stack);
            SP1 = pop(stack);
            if(SP1.type == TERMINAL) /* we are reducing the first wrapped item */
            {
                /* we only create the expression for the variable */
                var=(variable){.type=VAR_STRING, .no_touch=true, \
                    .value.string.data=SP0.content.term.value.ptr,\
                    .value.string.length=SP0.content.term.length+1};
                new_node->data.value = var;
                new_node->type = T_LITERAL;
                new_node->next=NULL;
                new_node->params=NULL;
                new_nonterminal.content.node = new_node;

                /* if there is still some wrapped stuff, we keep\
                    the '@' on the stack */
                if(token.type == TWrapString || token.type == TWrapIdent)
                {
                    push(stack,SP1);
                }
                push(stack,new_nonterminal);
            }
            else /* there is already something "wrapped" reduced */
            {
                /* first we reduce the string itself */
                var=(variable){.type=VAR_STRING, .no_touch=true, \
                    .value.string.data=SP0.content.term.value.ptr,\
                    .value.string.length=SP0.content.term.length+1};
                new_node->data.value = var;
                new_node->type = T_LITERAL;
                new_node->next = NULL;
                new_node->params=NULL;

                /* then we prepare the concatenation */
                tExpr *plus = ifj_malloc(sizeof(tExpr),PERSISTENT);

                plus->type=T_PLUS;
                plus->data.children[0] = SP1.content.node;
                plus->data.children[1] = new_node;
                plus->next = NULL;

                /* at end of wrapped sequence get rid of the '@'*/
                if(token.type != TWrapString && token.type != TWrapIdent)
                {
                    pop(stack);
                }

                /* and then we put the concatenation on the stack */
                new_nonterminal.content.node = plus;
                push(stack,new_nonterminal);
            }
            break;

        case TWrapIdent:
        /* strange extension <Expr> -> @"strangeness itself", part 2 */
            SP0 = pop(stack);
            SP1 = pop(stack);
            if(SP1.type == TERMINAL) /* there has to be a TWrapString after '@' */
            {
                fprintf(stderr,"TOpAt not followed by TWrapString\n");
                exit(ERR_42);
            }
            else /* there is already something "wrapped" reduced */
            {
                /* first we reduce the identifier itself */

                /* we check if the variable is already defined */
                unsigned **index = (unsigned **) bstLookUp(vartable,\
                                        SP0.content.term.value.ptr);
                /* if it is not, we optimisticly assume it is valid function name
                 * at a valid spot (both assumptions will be put to a test later)
                 *
                 * note that in this case it is SURE that an error will arise
                 * because the 'VAR_FUNCNAME' will be concatenated to string,
                 * which is illegal. But for design unity, this will not be
                 * detected any earlier then at type checking.
                 */
                if(index == NULL)
                {
                    var=(variable){.type=VAR_FUNCNAME, .no_touch=true};
                    new_node->data.value = var;
                    new_node->type = T_LITERAL;
                    addUndefSymbol(SP0.content.term);
                }
                /* if it is properly defined already, we set the expression
                 * node aproperietly */
                else
                {
                    new_node->type = T_VARIABLE;
                    new_node->data.index = **index;
                }
                    if(index == NULL)

                /* we set the expression node properly */
                new_node->next = NULL;
                new_node->params=NULL;

                /* now we prepare the concatenation */
                tExpr *plus = ifj_malloc(sizeof(tExpr),PERSISTENT);

                plus->type=T_PLUS;
                plus->data.children[0] = SP1.content.node;
                plus->data.children[1] = new_node;
                plus->next = NULL;

                /* if the wrapped sequence ends, we pop the '@' */
                if(token.type != TWrapString && token.type != TWrapIdent)
                {
                    pop(stack);
                }

                /* and finally we put the concatenation onto the stack */
                new_nonterminal.content.node = plus;
                push(stack,new_nonterminal);
            }
            break;

        case TLiteralNum:
            /* <Expr> -> 123.456e789
             * We only pop one item
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* constitute the constant carrying variable structure */
            var=(variable){.type=VAR_NUMERIC, .no_touch=true,\
                .value.numeric=SP0.content.term.value.num};
            new_node->data.value = var;
            new_node->type = T_LITERAL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TLiteralStr:
            /* <Expr> -> "blah blah blah"
             * Only one item needed to constitute the expression
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* this variable will hold the literal value */
            var=(variable){.type=VAR_STRING, .no_touch=true, \
                .value.string.data=SP0.content.term.value.ptr,\
                .value.string.length=SP0.content.term.length+1};
            new_node->data.value = var;
            new_node->type = T_LITERAL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_true:
            /* <Expr> -> true
             * we only pop the 'true' item
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* here the 'true' value is actually stored */
            var=(variable){.type=VAR_BOOL, .no_touch=true, \
                .value.boolean = true};
            new_node->data.value = var;
            new_node->type = T_LITERAL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_false:
            /* <Expr> -> false
             * we only pop the 'false' item
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* here the 'false' value is actually stored */
            var=(variable){.type=VAR_BOOL, .no_touch=true, \
                .value.boolean = false};
            new_node->data.value = var;
            new_node->type = T_LITERAL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_nil:
            /* <Expr> -> nil
             * we only pop the 'nil' item
             */
            SP0 = pop(stack);
            if(SP0.type != TERMINAL)
            {
                return false;
            }

            /* here the 'nil' value is actually stored */
            var=(variable){.type=VAR_NIL, .no_touch=true};
            new_node->data.value = var;
            new_node->type = T_LITERAL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpPlus:
            /* <Expr> -> <Expr> + <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            /* The new node shall know it is PLUS and what are the operands */
            new_node->type=T_PLUS;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpMinus:
            /* <Expr> -> <Expr> - <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_MINUS;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpMultiplicator:
            /* <Expr> -> <Expr> * <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_ASTERISK;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpDivider:
            /* <Expr> -> <Expr> / <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_SLASH;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpPower:
            /* <Expr> -> <Expr> ** <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_POWER;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpEqual:
            /* <Expr> -> <Expr> == <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_EQ;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpNotEqual:
            /* <Expr> -> <Expr> != <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_NE;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpGreater:
            /* <Expr> -> <Expr> > <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_GT;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpLess:
            /* <Expr> -> <Expr> < <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_LT;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpGreaterEqual:
            /* <Expr> -> <Expr> >= <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_GE;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpLessEqual:
            /* <Expr> -> <Expr> <= <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_LE;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_in:
            /* <Expr> -> <Expr> in <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_IN;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_notin:
            /* <Expr> -> <Expr> notin <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_NOTIN;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_not:
            /* <Expr> -> not <Expr>
             * ! this is an unary operation, so we pop only two items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            if(SP1.type != TERMINAL || SP0.type != NONTERMINAL)
            {
                return false;
            }
            new_node->type=T_NOT;
            new_node->data.children[0]=SP0.content.node;
            new_node->data.children[1]=NULL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TOpUnMinus:
            /* <Expr> -> - <Expr>
             * ! this is an unary operation, so we pop only two items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            if(SP1.type != TERMINAL || SP0.type != NONTERMINAL)
            {
                return false;
            }
            new_node->type=T_UMINUS;
            new_node->data.children[0]=SP0.content.node;
            new_node->data.children[1]=NULL;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_and:
            /* <Expr> -> <Expr> and <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_AND;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TKW_or:
            /* <Expr> -> <Expr> or <Expr>
             * it is binary operation, so we pop three items
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            new_node->type=T_OR;
            new_node->data.children[0]=SP2.content.node;
            new_node->data.children[1]=SP0.content.node;
            new_node->next=NULL;
            new_node->params=NULL;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TComma:
            /* This (legally) happens only in a function call or an array
             * definition. So all we do is we the 'next' pointer of the left
             * expression to the right pointer. Due to comma being a right
             * associative operator it is guaranteedto build a valid
             * parameters list.
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            SP2.content.node->next = SP0.content.node;
            new_node=SP2.content.node;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TColon:
            /* This only happens when indexating with range. According
             * to what is ASG for indexation expected to look like, we simply
             * connect the right-hand side of the colon to the left via the
             * 'next' pointer (in the same way as we do when reducing comma).
             *
             * The bad thing is, that we are supposed to support "incomplete"
             * indexations such as "[:4.001]", "[2.9999:]" and [:].
             */

/* this label is used by "incomplete indecation" correcture to pretend that
 * it has not happened at all.
 */
reduceColon:
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);

            /* here we correct the "incomplete indexation" of [<expr>:<nothing>]
             */
            if(SP0.type == TERMINAL)
            {
                /* first we prepare fake variable substituting the upper bound
                 *
                 * infinity is the proper value to put there for we want to tell
                 * the interpret that we want all the rest of the array /
                 * string
                 * but since the interpret requirements changed, the infinity
                 * get caught as soon as by synthesis
                 */

                var=(variable){.type=VAR_NUMERIC, .no_touch=true,\
                    .value.numeric=1.0/0.0};
                new_node->data.value = var;
                new_node->type = T_LITERAL;
                new_node->next=NULL;

                /* and then we pretend that this has never happened */
                new_nonterminal.content.node = new_node;
                push(stack,SP2);
                push(stack,SP1);
                push(stack,SP0);
                push(stack,new_nonterminal);
                new_node = ifj_malloc(sizeof(tExpr),PERSISTENT);
                goto reduceColon;
            }

            /* here we correct the "incomplete indexation" of [<nothing>:<expr>]
             */
            else if(SP2.type == TERMINAL)
            {
                /* first we prepare fake variable substituting the upper bound
                 *
                 * here we put zero which is natural denominator of the first
                 * array member
                 */
                var=(variable){.type=VAR_NUMERIC, .no_touch=true,\
                    .value.numeric=0.0};
                new_node->data.value = var;
                new_node->type = T_LITERAL;
                new_node->next=NULL;
                new_node->params=NULL;
                
                /* and then we pretend that this has never happened */
                new_nonterminal.content.node = new_node;
                push(stack,SP2);
                push(stack,new_nonterminal);
                push(stack,SP1);
                push(stack,SP0);
                new_node = ifj_malloc(sizeof(tExpr),PERSISTENT);
                goto reduceColon;
            }

            /* an error might have occured as well  */
            else if(SP2.type != NONTERMINAL || SP1.type != TERMINAL\
                || SP0.type != NONTERMINAL)
            {
                return false;
            }

            SP2.content.node->next = SP0.content.node;


            new_node=SP2.content.node;
            new_nonterminal.content.node = new_node;
            push(stack,new_nonterminal);
            break;

        case TRightParenthesis:
            /* Here we finally get into some HC stuff. The ')' symbol may
             * indice (error,) precedence parenthesises or a function call.
             * But function call has two variants - with parameters and without.
             * These have to be threated seperately, so we have three different
             * rules to choose from here.
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            new_node->next=NULL;

            /* First, it may be a function call without parameters */
            if(SP2.type == TERMINAL && SP1.type == TERMINAL &&\
                SP0.type == TERMINAL)
            {
                /* we check wheter it is builtin-function call*/
                intFunction int_function = \
                    builtinFunction(SP2.content.term.value.ptr);

                /* if it is not, we store the function call in the function call
                 list */
                if(int_function==FUNC_NOT_BUILTIN)
                {
                    new_node->type = T_FUNCCALL;
                    TFunctionCall func_call;
                    func_call.generator = SP2.content.term;
                    func_call.expr = new_node;
                    addFunctionCall(func_call);
                }
                /* if it is, we directly transform it into an executable form */
                else
                {
                    new_node->type=T_INTFUNC;
                    new_node->data.index=int_function;
                }
                new_node->params = NULL;
                /* finnaly we push it back onto the stack */
                new_nonterminal.content.node=new_node;
                push(stack,new_nonterminal);
            }
            /* then it might be an error */
            else if(SP2.type != TERMINAL || SP1.type != NONTERMINAL\
                || SP0.type != TERMINAL)
            {
                return false;
            }
            /* otherwise we expect it to be function call with parameters */
            else
            {
                SP0 = pop(stack); /* but we have to reach for next item
                on the pushdown*/
                /* and see whether it was an identifier - suspected funtion name */
                if(SP0.type != TERMINAL || SP0.content.term.type != TIdentifier)
                { /* it was not an identifier */
                    push(stack,SP0);

                    new_node = SP1.content.node;
                    new_nonterminal.content.node = new_node;
                    push(stack,new_nonterminal);
                }
                else
                { /* it was an identifier -- it is function call */

                    /* prepare the function parameters */
                    new_node->params = SP1.content.node;
                    /* check whether the function is builtin */
                    intFunction int_function = \
                        builtinFunction(SP0.content.term.value.ptr);

                    /* behave according to the result (see zero params func call)*/
                    if(int_function==FUNC_NOT_BUILTIN)
                    {
                        new_node->type = T_FUNCCALL;
                        TFunctionCall func_call;
                        func_call.generator = SP0.content.term;
                        func_call.expr = new_node;
                        addFunctionCall(func_call);
                    }
                    else
                    {
                        new_node->type = T_INTFUNC;
                        new_node->data.index=int_function;
                    }

                    /* finnaly push the synthetised expression onto the stack */
                    new_nonterminal.content.node = new_node;
                    push(stack,new_nonterminal);
                }
            }
            break;

        case TRightBracketSquare:
            /* Here we've get another piece of the HC stuff. The ']' symbol may
             * indice (error,) array definition or indexing. This section
             * construction is similar to the reduction of ')'.
             */
            SP0 = pop(stack);
            SP1 = pop(stack);
            SP2 = pop(stack);
            new_node->next=NULL;
            new_node->params=NULL;

            /* Indexation requieres parameter and empty arrays are not allowed */
            if(SP2.type != TERMINAL || SP1.type != NONTERMINAL\
                || SP0.type != TERMINAL)
            {
                fprintf(stderr,"generic bracketing error\n");
                return false;
            }
            else
            {
                /* but we have to reach for next item on the pushdown and see
                 * whether it was an expression - suspected array or string
                 * to be indexed */
                SP0 = pop(stack);
                if(SP0.type != NONTERMINAL)
                { /* it was not an expression, therefore we are coping with array
                   * definition */
                    push(stack,SP0);

                    new_node->type = T_INTFUNC;
                    new_node->data.index = FUNC_ARRAYINIT;
                    new_node->params = SP1.content.node;
                    new_nonterminal.content.node = new_node;
                    push(stack,new_nonterminal);
                }
                else
                { /* it was an expression -- it is indexation */
                    /* it may be one-parameter indexation that is accessing an element */
                    if(SP1.content.node->next==NULL)
                    {
                        new_node->type = T_INTFUNC;
                        new_node->data.index = FUNC_ACCESS;
                        SP0.content.node->next = SP1.content.node;
                        new_node->params = SP0.content.node;
                        new_nonterminal.content.node = new_node;
                        push(stack,new_nonterminal);
                    }
                    /* or accessing range */
                    else
                    {
                        /* if the upper bound equals to infinity, we shall unlist it.
                         * This is required by the interpret because infinity could arise
                         * from invalid user code but would be accepted which is undesirable.
                         *
                         * Note that this causes a memory leak, but it is acceptable for it
                         * allows us to keep the memory module simple and efficient and we
                         * also see clearly that this leak size is not a byte greater than
                         * O(n) where n is number of '[whaterever:]' indexations.
                         */
                        if(SP1.content.node->next->data.value.type == VAR_NUMERIC &&\
                           SP1.content.node->next->data.value.value.numeric == (1.0/0.0))
                        {
                            SP1.content.node->next = NULL;
                        }

                        new_node->type = T_INTFUNC;
                        new_node->data.index = FUNC_SUBVAR;
                        SP0.content.node->next = SP1.content.node;
                        new_node->params = SP0.content.node;
                        new_nonterminal.content.node = new_node;
                        push(stack,new_nonterminal);
                    }
                }
            }
            break;

        /* if this happens, go out, look for a nice tree and hang yourself up there */
        default:
            fprintf(stderr,"[%s , %s] defaulted\n",token_text[um_terminal.type],\
                token_text[token.type]);
            exit(ERR_42);

    }
    return true;
}

/** Parses <Expression>.
 *
 * This is the 'parental' function of the whole precedence parser. It handles
 * the pushdown operations, having the applyReduce() apply the reductions.
 * No matter what happens, it leaves the first EOL it encounters in the input
 * token stream.
 *
 * @param vartable pointer to a vartable of the function the expression is in
 * @returns pointer to the tree root or NULL
 */
tExpr *expression(tBSTNode *vartable)
{
    TToken token, um_terminal; // actually read, uppermost terminal
    TExprTermClass class;
    TPushdownItem *stack=NULL,*ptr;

    /* pushdown initialization */
    TPushdownItem item;
    item.type = BOTTOM;
    item.content.term.type = TUndefined;
    push(&stack,item);
    um_terminal = stack->content.term;

    tExpr *new_node;
    bool retval=true;

    /* reading loop */
    while(1)
    {
        token = bufferedToken();
        class = classifyToken(token);

        if(class == ILLEGAL)
        {
            parserError(ILL_EXPR_TOKEN,token,TUndefined);
            retval = false;
            break;
        }

        /* Auxilliary output demonstratin the proper function */
/*        printf("%u:%u [%s,%s]\n", token.line, token.col,
            token_text[um_terminal.type],token_text[token.type]); */

        /* According to the precedence table, do */
        switch(precedence_table[um_terminal.type][token.type])
        {
            case ERROR:
                parserError(PREC_TABLE_ERROR,token,um_terminal.type);
                retval = false;
                goto eatTillEOExpr;
            case SHIFT:
                item.type = TERMINAL;
                item.content.term = token;
                push(&stack,item);
                break;
            case REDUCE:
                /* prepare space for a new expression node */
                new_node = ifj_malloc(sizeof(tExpr),PERSISTENT);

                if(!applyReduce(&stack,token,um_terminal,new_node,vartable))
                {
                    parserError(RULE_UNAPPLIABLE,token,TUndefined);
                    retval = false;
                    goto eatTillEOExpr;
                }
                returnToken();
                break;
            case SUCCESS:
                goto eatTillEOExpr;
                break;
            default:
                fprintf(stderr,"This simply doesn't happen\n");
                exit(ERR_42);
                break;
        }

/*        ptr = stack;
        while(ptr!=NULL)
        {
            printPushdownItem(stdout, *ptr);
            ptr=ptr->next;
            printf(" ");
        }
        printf("\n");*/

        /* search for the new uppermost terminal */
        ptr = stack;
        while(ptr->type == NONTERMINAL)
        {
            ptr = ptr->next;
            if(ptr == NULL)
            {
               fprintf(stderr,"reached bottom during searching for terminal\n");
                exit(ERR_42);
            }
        }
        um_terminal = ptr->content.term;
    }

    /* ensure there is EOL waiting for the calling function */
eatTillEOExpr:
    while(token.type != TEOL && token.type != TAssignment)
    {
        token=bufferedToken();
    }
    returnToken(); // we want the EOL to wait there for the caller

    tExpr *expr_root;
    if(retval)
    {
        expr_root = stack->content.node;
    }

    if(retval == true)
    {
//        printf("expression accepted\n");
        return expr_root;
    }
    else
    {
        return NULL;
    }
}

/* auxiliary function used to build a 'literal' expression from a token */
tExpr *literalExpression(TToken token)
{
    tExpr *literal = ifj_malloc(sizeof(tExpr),PERSISTENT);

    literal->next = NULL;
    literal->data.children[0] = NULL;
    literal->data.children[1] = NULL;
    literal->type = T_LITERAL;

    variable var;

    switch(token.type)
    {
        case TKW_nil:
            var=(variable){.type=VAR_NIL, .no_touch=true};
            literal->data.value = var;
            break;
        case TKW_false:
            var=(variable){.type=VAR_BOOL, .no_touch=true,\
                .value.boolean = false};
            literal->data.value = var;
            break;

        default:
            fprintf(stderr,"We don't support %s literals\n",\
                        token_text[token.type]);
            exit(ERR_42);
    }
    return literal;
}

/** Parses and synthetises the <If Else> statement.
 *
 * Creates following structure:
 *                      Pass ------------------ Command List
 *                     / (then)                              \
 * predecessor - IfElse                                       Pass
 *                     \                                     /
 *                      Elif ---------- Command List -------+
 *                          \                               /
 *                          Elif --------- Command List  --+
 *                             \                          /
 *                              --- Command List --------/
 *
 *
 * Which ensures it look linear for both predecessor and succesor
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the unifying PASS command or NULL
 */
tCommand *ifElse(tCommand *predecessor, tBSTNode *vartable, unsigned *vartable_tsp)
{
    TToken token;
    tExpr *cond=NULL;

    tCommand *ifels = ifj_malloc(sizeof(tCommand),PERSISTENT);
    tCommand *then_pass = ifj_malloc(sizeof(tCommand),PERSISTENT);
    tCommand *else_pass = ifj_malloc(sizeof(tCommand),PERSISTENT);

    *then_pass = (tCommand) {.type=COMM_PASS};
    *else_pass = *then_pass;

    *ifels = (tCommand) {.type = COMM_IFELS, .next=then_pass};
    predecessor->next = ifels;

    tCommand *ifels_end = ifj_malloc(sizeof(tCommand),PERSISTENT);

    *ifels_end = (tCommand) {.type = COMM_PASS};

    token = bufferedToken();
    if(token.type != TKW_if)
    {
        /* This should generally NOT happen */
        return NULL;
    }

    cond = expression(vartable);
    if(cond == NULL)
    {
        return NULL;
    }
    ifels->expression = cond;
    addExpressionTree(cond,token.line);

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    tCommand *then_end = commandList(then_pass,vartable,vartable_tsp);
    if(then_end == NULL)
    {
        return NULL;
    }
    then_end->next = ifels_end;

    tCommand *elif,*elif_end,*last_elif=NULL;
    while((token = bufferedToken()).type == TKW_elif)
    {
        elif = ifj_malloc(sizeof(tCommand),PERSISTENT);
        elif->type = COMM_IFELS;
        if(last_elif != NULL)
        {
            last_elif->alternative=elif;
        }
        else
        {
            ifels->alternative=elif;
        }
        elif->expression = expression(vartable);
        token = bufferedToken();
        if(token.type != TEOL)
        {
            parserError(UNEXPECTED,token,TEOL);
            return NULL;
        }

        if(elif->expression==NULL)
        {
            return NULL;
        }
        elif_end = commandList(elif,vartable,vartable_tsp);
        if(elif_end == NULL)
        {
            return NULL;
        }
        elif_end->next = ifels_end;
        last_elif=elif;
    }
    /* at this place, token is first token after last valid elif branch */

    if(token.type==TKW_end)
    {
        token = bufferedToken();
        if(token.type != TEOL)
        {
            parserError(UNEXPECTED,token,TEOL);
            return NULL;
        }

        if(last_elif == NULL)
        {
            ifels->alternative = ifels_end;
        }
        else
        {
            last_elif->alternative = ifels_end;
        }
        return ifels_end;
    }

    /* at this place we went over all valid elif branches and they were not
     * closed by 'end' thus we expect the else branch as the only possibility
     */

    if(token.type != TKW_else)
    {
        parserError(UNEXPECTED,token,TKW_else);
        return NULL;
    }
    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    if(last_elif == NULL)
    {
        ifels->alternative = else_pass;
    }
    else
    {
        last_elif->alternative = else_pass;
    }

    tCommand *else_end = commandList(else_pass,vartable,vartable_tsp);
    if(else_end == NULL)
    {
        return NULL;
    }
    else_end->next = ifels_end;

    token = bufferedToken();
    if(token.type != TKW_end)
    {
        parserError(UNEXPECTED,token,TKW_end);
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    return ifels_end;
}

/** Parses and synthetises <Command List>.
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the last command of the list or NULL
 */
tCommand *commandList(tCommand *predecessor, tBSTNode *vartable, unsigned *vartable_tsp)
{
    TToken token;

    while(token = bufferedToken(),token.type!=TKW_end && token.type!=TKW_else &&\
        token.type!=TKW_elif)
    {
        returnToken();
        predecessor = command(predecessor, vartable, vartable_tsp);
        if(predecessor == NULL)
        {
            return NULL;
        }
    }

    returnToken();
    return predecessor;

}

/** Parses and synthetises the <While> statement.
 *
 * Creates a following structure:
 *
 *                      Loop body (command list)
 *                     /                        \
 * predecessor - IfElse  <- <- <- <- <- <- <- <- .
 *                     \
 *                      Pass - succesor
 *
 * , which ensures it look linear for both predecessor and successor.
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the exiting Pass or NULL
 */
tCommand *whileStatement(tCommand *predecessor, tBSTNode *vartable,\
            unsigned *vartable_tsp)
{
    TToken token;

    tCommand *while_statement = ifj_malloc(sizeof(tCommand),PERSISTENT);
    while_statement->type = COMM_IFELS;

    token = bufferedToken();
    if(token.type != TKW_while)
    {
        return NULL;
    }

    tExpr *cond=expression(vartable);
    if(cond == NULL)
    {
        return NULL;
    }
    while_statement->expression = cond;
    addExpressionTree(cond,token.line);


    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    tCommand *body_last = commandList(while_statement,vartable,vartable_tsp);
    if(body_last == NULL)
    {
        return NULL;
    }

    tCommand *end_pass = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *end_pass =(tCommand){.type=COMM_PASS};
    body_last->next = while_statement;
    while_statement->alternative = end_pass;

    token = bufferedToken();
    if(token.type != TKW_end)
    {
        parserError(UNEXPECTED,token,TKW_end);
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    predecessor->next = while_statement;
    return end_pass;
}

/** Parses and synthetises the <Return> statement.
 *
 * Note that we continue parsing of after the return command even though it is
 * an evident dead code. We are expected to detect any faults in script!
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the return command or NULL
 */
tCommand *returnStatement(tCommand *predecessor, tBSTNode *vartable)
{
    TToken token;

    tCommand *return_statement = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *return_statement = (tCommand){.type=COMM_RETURN};

    token = bufferedToken();
    if(token.type != TKW_return)
    {
        parserError(UNEXPECTED,token,TKW_return);
        return NULL;
    }

    tExpr *retval = expression(vartable);
    if(retval == NULL)
    {
        return NULL;
    }
    return_statement->expression = retval;
    addExpressionTree(retval,token.line);

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    predecessor->next = return_statement;
    return return_statement;
}

/** Parser and sythetises the <Assingment> statement.
 *
 * This function is responsible for inserting newly defined variables in the
 * vartable (which is a reason why we propagate (vartable_)tsp as a pointer).
 *
 * Note that explicit function call will be wrapped in this,
 * i.e. 'nothing = print("Hello")'
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the synthetised assignment command or NULL
 */
tCommand *assignment(tCommand *predecessor, tBSTNode *vartable, unsigned *tsp)
{
    TToken token;

    tCommand *assign = ifj_malloc(sizeof(tCommand),PERSISTENT);
    assign->type=COMM_ASSIGNMENT;
    assign->next=NULL;

    /* ARRAY implies we support general l-values. Therefore we parse the left
     * side of an assignment as if it were an expression. Of course only a small
     * subset of expressions forms valid l-values.
     */

    /* In case the 'l-value' is an everyday variable, we need to know it even
     * after parsing the 'l-expression', therefore we store it in 'token'.
     */
    token = bufferedToken();
    returnToken();

    assign->lvalue = expression(vartable);
    if(assign->lvalue == NULL)
    {
        fprintf(stderr,"Failed to parse l-value at line %u\n",token.line);
        exit(SYNTAX_ERROR);
    }

//printf("parsed l-expression\n");

    /* Now we have several options of what does the expression look like:
     *  + it is variable, therefore we are assigning into an already known
     *    variable
     *  + it is a "funcname" literal which denotes assignment into a new
     *    variable
     *  + it is a call of subVar() or accesElement(), then it is legal l-value
     *  + it is something else, such as literal, addition, function call, etc.
     *    which is an invalid l-value
     * note
     */

    if(assign->lvalue->type==T_VARIABLE)
    {
        /* since we have ordinary assignment, we want the destination
         * in the 'index' while keeping the 'lvalue' pointer at NULL
         */
//printf("known var\n");
        assign->index = assign->lvalue->data.index;
        assign->lvalue = NULL; /* small and acceptable memory leak (upto
            n_assignments_in_script*sizeof(tExpr)) */
    }
    else if (assign->lvalue->type==T_LITERAL &&\
            assign->lvalue->data.value.type==VAR_FUNCNAME)
    {
//printf("new var\n");
        /* add it to the list of variables to be checked for conflicting
         * names (to a name of some function)
         */
        addVarName(token);
        /* create it in the BST_INSERT memory class -- it will be only accesed
         * via the bst, therefore there is no use for it after the bst is freed
         */
        unsigned *new_index = ifj_malloc(sizeof(unsigned),BST_INSERT);

        /* prepare vartable index */
        *new_index = *tsp;
        (*tsp)++;
        assign->index=*new_index;

        bstInsert(vartable,token.value.ptr,new_index);

        assign->index = *new_index;
        assign->lvalue = NULL;

        /* we don't want it to be checked for being undefined -- we are
         * defining it here!
         */
         if(usl_tail->prev==NULL)
         {
            usl_tail = NULL;
            usl_head = NULL;
         }
         else
         {
            usl_tail = usl_tail->prev;
            usl_tail->next = NULL;
         }
    }
    else if(assign->lvalue->type == T_INTFUNC &&
            (assign->lvalue->data.index == FUNC_SUBVAR ||
             assign->lvalue->data.index == FUNC_ACCESS))
    {
//printf("valid l-value\n");
        /* we do nothing */;
    }
    else
    {
        fprintf(stderr,"Invalid l-value at line %u\n",token.line);
        exit(SYNTAX_ERROR);
    }

    token = bufferedToken();
    if(token.type != TAssignment)
    {
        parserError(UNEXPECTED,token,TAssignment);
        return NULL;
    }

//printf("passed the '='\n");

    tExpr *aux=expression(vartable);
    if(aux == NULL)
    {
        return NULL;
    }
    assign->expression = aux;
    addExpressionTree(aux,token.line);

//printf("parsed r-expression\n");

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return NULL;
    }

    predecessor->next = assign;
    return assign;
}

/** Parses the <For Do> statement.
 *
 * @returns whether the parsing was succesful
 */
tCommand *forDo(tCommand *predecessor, tBSTNode *vartable,\
                unsigned *vartable_tsp)
{
    TToken token;

    token = bufferedToken();
    if(token.type != TKW_for)
    {
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TIdentifier)
    {
        parserError(UNEXPECTED,token,TIdentifier);
        return NULL;
    }
    
    /* this is the vartable index of the 'item' in 
     * 'for item in <Expr> EOL <Command List> end EOL'
     */ 
    unsigned iterator_index; 

    /* we check if the variable is already defined */
    unsigned **index = (unsigned**) bstLookUp(vartable,token.value.ptr);
    if(index==NULL)
    {
        /* prepare vartable index in the BST_INSERT memory class */
        unsigned *new_index = ifj_malloc(sizeof(unsigned),BST_INSERT);
        *new_index = *vartable_tsp;
        (*vartable_tsp)++;
        /* and insert it */
        bstInsert(vartable,token.value.ptr,new_index);

        iterator_index=*new_index;
    }
    else
    {
        iterator_index=**index;
    }


    token = bufferedToken();
    if(token.type != TKW_in)
    {
        parserError(UNEXPECTED,token,TKW_in);
        return NULL;
    }

    /* We prepare auxilliary variable holding the read-only copy of the range
     * to iterate over. This has to be done in the run time, therefore we have
     * to synthetise assignment command. The read-only-ness is achieved by simply
     * not giving a name to this variable (it is not added into the vartable BST).
     */
    tExpr *range = expression(vartable);
    if(range == NULL)
    {
        return NULL;
    }
    tCommand *copy_range = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *copy_range = (tCommand){.type=COMM_ASSIGNMENT, .index=*vartable_tsp,\
                    .lvalue=NULL, .expression=range};
    (*vartable_tsp)++;
    predecessor->next = copy_range;
    

    /* We initialize the index that will iterate through the array to zero.
     */
    tExpr *zero_expr = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *zero_expr = (tExpr){ .type=T_LITERAL, .params=NULL, .next=NULL, .data.value=\
            (variable){.type=VAR_NUMERIC, .no_touch=true, .value.numeric = 0.0}};

    tCommand *counter_init = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *counter_init = (tCommand){.type=COMM_ASSIGNMENT, .index=*vartable_tsp,\
                    .lvalue=NULL, .expression=zero_expr};
    (*vartable_tsp)++;
    copy_range->next = counter_init;


    /* Here we prepare the 'loop predicate' :-)
     */
    tExpr *read_only_copy = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *read_only_copy = (tExpr){.type=T_VARIABLE, .data.index=copy_range->index};

    tExpr *range_len = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *range_len = (tExpr){.type=T_INTFUNC, .params=read_only_copy,\
                    .data.index=FUNC_LEN};
    
    tExpr *counter = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *counter = (tExpr){.type=T_VARIABLE, .data.index=counter_init->index};

    tExpr *condition = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *condition = (tExpr){.type=T_LT, .data.children[0]=counter, \
                    .data.children[1]=range_len};
    
    tCommand *for_loop = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *for_loop = (tCommand){.type=COMM_IFELS,.expression=condition};
    counter_init->next = for_loop;


    /* and we eat the EOL ending the range specification */
    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED, token, TEOL);
        return NULL;
    }

    /* first thing to be done in the loop is moving the iterator one element
     * forward
     */

    tExpr *ro_accessed = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *ro_accessed = (tExpr){.type=T_VARIABLE, .data.index=copy_range->index,\
                            .params=NULL, .next=counter};
    tExpr *index_funccall = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *index_funccall = (tExpr){.type=T_INTFUNC, .data.index=FUNC_ACCESS,\
                            .next=NULL, .params=ro_accessed};
    tCommand *indexation = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *indexation = (tCommand){.type=COMM_ASSIGNMENT, .index=iterator_index,\
                .lvalue=NULL, .expression=index_funccall};
    for_loop->next=indexation;

    tCommand *loop_body=commandList(indexation,vartable,vartable_tsp);
    if(loop_body == NULL)
    {
        return NULL;
    }

    tExpr *one_expr = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *one_expr = (tExpr){ .type=T_LITERAL, .params=NULL, .next=NULL, .data.value=\
            (variable){.type=VAR_NUMERIC, .no_touch=true, .value.numeric = 1.0}};
    
    tExpr *increment = ifj_malloc(sizeof(tExpr),PERSISTENT);
    *increment = (tExpr){.type=T_PLUS, .data.children[0]=counter,\
                    .data.children[1]=one_expr};

    tCommand *iterate = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *iterate = (tCommand){.type=COMM_ASSIGNMENT, .next=for_loop,\
                    .expression=increment, .index=counter_init->index};
    loop_body->next = iterate;

    token = bufferedToken();
    if(token.type != TKW_end)
    {
        parserError(UNEXPECTED, token, TKW_end);
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED, token, TKW_end);
        return NULL;
    }

    tCommand *exiting_pass = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *exiting_pass = (tCommand){.type=COMM_PASS};
    for_loop->alternative=exiting_pass;

    return exiting_pass;
}

/**  To be commented later *PROCRASTINATION*.
 * Parses the <Loop> statement
 *
 *                                                 (then)
 * predecessor -> pass -> command list -> IfElse ---->---> Succesor
 *                  ^                      |
 *                  |                      | (else)
 *                  ^--<---<---<----<---<- .
 *
 *
 * @returns whether the parsing was successful
 */
tCommand *loop(tCommand *predecessor, tBSTNode *vartable,\
                unsigned *vartable_tsp)
{
    TToken token;

    token = bufferedToken();
    if(token.type != TKW_loop)
    {
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TEOL)
    {
        return NULL;
    }

    tCommand *pass = ifj_malloc(sizeof(tCommand),PERSISTENT);
    *pass=(tCommand){.type=COMM_PASS};
    predecessor->next = pass;

    tCommand *body_last = commandList(pass,vartable,vartable_tsp);
    if(body_last == NULL)
    {
        return NULL;
    }

    token = bufferedToken();
    if(token.type != TKW_end)
    {
        return NULL;
    }

    tCommand *loop_condition = ifj_malloc(sizeof(tCommand),PERSISTENT);
    body_last->next = loop_condition;

    token = bufferedToken();
    if(token.type != TEOL)
    {
        *loop_condition = (tCommand){.type=COMM_IFELS, .alternative=pass};
        returnToken();
        loop_condition->expression = expression(vartable);
        if(loop_condition->expression == NULL)
        {
            return NULL;
        }
        addExpressionTree(loop_condition->expression,token.line);

        token = bufferedToken();
        if(token.type != TEOL)
        {
            return NULL;
        }
    }
    else /* beware that this is equal to 'end false EOL' */
    {
        /* synthetise an infinite loop */
        *loop_condition=(tCommand){.type=COMM_IFELS, .expression = literalExpression(\
            (TToken){.type = TKW_false}), .alternative=pass};
    }
    return loop_condition;
}

/** Parses and synthetises a <Command>.
 *
 * This is where we decide whether is a simple assignment or
 * some control statement. An empty line line is simply treated as if it didn't
 * exist at all.
 *
 * @param predecessor pointer to the previous command
 * @param vartable pointer to the actual function vartable
 * @param vartable_tsp pointer to the number of actually defined variables
 * @returns pointer to the synthetised command or NULL
 */
tCommand *command(tCommand *predecessor, tBSTNode *vartable,\
                    unsigned *vartable_tsp)
{
    TToken token;
    token = bufferedToken();
    tCommand *successor;
    if(token.type == TIdentifier)
    {
        returnToken();
        successor = assignment(predecessor, vartable, vartable_tsp);
/*        if(successor)
        {
            printf("Assignment at line %u\n",token.line);
        }*/
        return successor;
    }
    else if(token.type == TEOL)
    {
//        printf("Line %u is empty\n", token.line);
        return predecessor;
    }
    else if(token.type == TKW_if)
    {
        returnToken();
//        printf("if encountered at line %u\n", token.line);
        successor = ifElse(predecessor, vartable, vartable_tsp);
/*        if(successor)
        {
            printf("If-else statement from line %u trough ",token.line);
            token = bufferedToken();
            printf("%u\n",token.line);
            returnToken();
        }*/
        return successor;
    }
    else if(token.type == TKW_while)
    {
        returnToken();
//        printf("while encountered at line %u\n", token.line);
        successor = whileStatement(predecessor, vartable, vartable_tsp);
/*        if(successor)
        {
            printf("While statement from line %u trough ",token.line);
            token = bufferedToken();
            printf("%u\n",token.line);
            returnToken();
        }*/
        return successor;
    }
    else if(token.type == TKW_return)
    {
        returnToken();
        successor = returnStatement(predecessor, vartable);
        if(successor)
        {
//            printf("Return at line %u\n",token.line);
            return successor;
        }
    }
    else if(token.type == TKW_loop)
    {
        returnToken();
        successor = loop(predecessor, vartable, vartable_tsp);
        if(successor)
        {
//            printf("Loop statement from line %u trough ",token.line);
//            token = bufferedToken();
//            printf("%u\n",token.line);
//            returnToken();
            return successor;
        }
    }
    else if(token.type == TKW_for)
    {
        returnToken();
       fprintf(stderr,"for encountered at line %u\n", token.line);
//        printf("unsupported\n");
//        return NULL;
        successor = forDo(predecessor, vartable, vartable_tsp);
        if(successor)
        {
            fprintf(stderr,"fordo statement from line %u trough ",token.line);
            token = bufferedToken();
           fprintf(stderr,"%u\n",token.line);
            returnToken();
            return successor;
        }
    }
    else
    {
        parserError(OTHER,token,TUndefined);
        if(token.type == TEOF)
        {
            exit(SYNTAX_ERROR);
        }
    }

   fprintf(stderr,"Command at line %u not parsed\n",token.line);

    /* get rid of the rest of the line */
    while(token.type != TEOL)
        token = bufferedToken();

    return NULL;
}

/** Parses <Formal Parameters> and stores relevant info in the vartable.
 *
 * The 'relevant info' stored are actually only names of the parameters and
 * their total number.
 *
 * @param vartable pointer to the just-defined function vartable
 * @param vartable_tsp pointer to place where number of parameters is stored
 * @returns whether the parsing was successful
 */
bool formalParameters(tBSTNode *vartable, unsigned *vartable_tsp)
{
    TToken token;

    bool last_was_comma=false;

    while(true)
    {
        token = bufferedToken();
        if(token.type == TIdentifier) /* there are some parameters */
        {
            unsigned **ptr= (unsigned **) bstLookUp(vartable,token.value.ptr);
            if(ptr!=NULL)
            {
                parserError(TWO_SAME_FORMAL_PARAMETERS,token,TUndefined);
            }
            unsigned *index = ifj_malloc(sizeof(unsigned),BST_INSERT);
            *index = *vartable_tsp;

            intFunction int_function = builtinFunction(token.value.ptr);
            if(int_function != FUNC_NOT_BUILTIN)
            {
                parserError(BUILTIN_FUNC_VAR,token,TUndefined);
                return false;
            }

            bstInsert(vartable,token.value.ptr,index);
            (*vartable_tsp)++;

            token = bufferedToken();
            if(token.type == TComma)
            {
                last_was_comma=true; /* continue to next iteration */
            }
            else if(token.type == TRightParenthesis)
            {
                returnToken();
                return true;
            }
            else /* after a formal parameter we want ')' or ',' */
            {
                parserError(UNEXPECTED,token,TComma);
                return false;
            }
        }
        else if(token.type == TRightParenthesis && !last_was_comma) /* there are no parameters */
        {
            returnToken();
            return true;
        }
        else /* there is an error in the parameters definition */
        {
            parserError(OTHER,token,TUndefined);
            return false;
        }
    }
    returnToken();
    return true;
}

/** Parses and sythetises a <Function>.
 *
 * @returns whether the parsing was successful
 */
bool functionDefinition(void)
{
    TToken token;

    /* Establish a function prototype */
    tFunc *function = ifj_malloc(sizeof(tFunc),PERSISTENT);



    token = bufferedToken();
    if(token.type != TKW_function)
    {
        return false;
    }

    token = bufferedToken();
    if(token.type != TIdentifier)
    {
        parserError(UNEXPECTED,token,TIdentifier);
        return false;
    }
    char *name = token.value.ptr;
    
    if(bstLookUp(functable,name)!=NULL)
    {
        fprintf(stderr,"attempt to redefine function %s() at line %u\n",\
                    name,token.line);
        exit(MISC_SEMANTIC_ERROR);
    }

    /* tell the memory module we're going into a function */
    set_IF_flag(); 
    
    /* Establish a vartable */
    tBSTNode *vartable=bstCreateTree();
    if(vartable == NULL)
    {
        printf("bst creation failed\n");
        exit(INTERNAL_ERROR);
    }
    
    /* prepare space where current number of defined variables will be stored */
    unsigned *vartable_tsp = ifj_malloc(sizeof(unsigned),BST_INSERT);
    *vartable_tsp = 0;
    
    token = bufferedToken();
    if(token.type != TLeftParenthesis)
    {
        parserError(UNEXPECTED,token,TLeftParenthesis);
        return false;
    }

    if(!formalParameters(vartable,vartable_tsp))
    {
        return false;
    }
    function->paramnumber = *vartable_tsp;

    token = bufferedToken();
    if(token.type != TRightParenthesis)
    {
        /* this should generally not happen */
        parserError(UNEXPECTED,token,TRightParenthesis);
        return false;
    }

    /* prepare a Pass for the commands to have a predecessor */
    tCommand *starting_pass = ifj_malloc(sizeof(tCommand),PERSISTENT);
    starting_pass->type = COMM_PASS;
    function->commands = starting_pass;

    tCommand *end = commandList(starting_pass,vartable,vartable_tsp);
    if(end == NULL)
    {
        return false;
    }

    token = bufferedToken();
    if(token.type != TKW_end)
    {
        parserError(UNEXPECTED,token,TKW_end);
        return false;
    }

    token = bufferedToken();
    if(token.type != TEOL)
    {
        parserError(UNEXPECTED,token,TEOL);
        return false;
    }

    function->vartable_size = *vartable_tsp;

    /* tell memory manager we're leaving a function */
    clear_IF_flag();
    /* and that we need not anymore its vartable (including the referenced 
     * unsigneds) */
    stash_temp();

    /* insert the function in the functable */
    bstInsert(functable,name,function);

/*    printf("synthetised function '%s', %u params, %u variables at %p\n",
            name,function->paramnumber,*vartable_tsp - function->paramnumber,
            function); */
/*    printf("bst stored %p\n",*bstLookUp(functable,name)); */

    return true;
}

/** Parses <Main Item>.
 *
 * returns whether the parsing was succesful
 */
bool mainItem(void)
{
    TToken token;
    token = bufferedToken();

    /* <main item> is either a function definition or a main() command*/
    if(token.type == TKW_function)
    { /* for funtion, have it synthetised */
//        printf("Function definition encountered at line %u\n", token.line);
        returnToken();
        if(functionDefinition())
        {
//            printf("Function from line %u trough ",token.line);
            returnToken();
            token=bufferedToken();
//            printf("%u\n",token.line);
            returnToken();
            return true;
        }
    }
    else
    { /* for an ordinary command, have it synthetised and enlist it in the main() */
        returnToken();
        last_main_command = command(last_main_command,main_vartable,&main_tsp);
        if(last_main_command != NULL)
        {
            return true;
        }
    }

    return false;
}

/** Parses <Main Item List>.
 *
 * Implements the outermost loop which handles whole the token stream.
 *
 * @returns wheter the parsing was successful
 */

extern tBSTNode *bstCreateTree();

bool mainItemList(void)
{
    TToken token;
    /* have the main() begin at already created Pass */
    last_main_command = ((tFunc*) start_point)->commands;

    /* prepare vartable for the main() */
    main_vartable = bstCreateTree();

    /* initialize functable -- note it doesn't survive the parsing */
    functable = bstCreateTree();

    /* keep reading tokens and have them parsed one by one */
    while(token=bufferedToken(),token.type != TEOF)
    {
        switch(token.type)
        {
            case TEOL:
            case TIdentifier:
            case TKW_function:
            case TKW_if:
            case TKW_while:
            case TKW_return:
            case TKW_loop:
            case TKW_for:
                returnToken();
                if(mainItem() == false)
                {
                    return false;
                }
                break;
            default:
                parserError(OTHER,token,TUndefined);
                exit(SYNTAX_ERROR);
        }
    }

    return true;
}

/** Enterance point of syntax parser.
 *
 * @returns 1 on succes, 0 otherwise
 */
int parseSource(void)
{
    int result;

    /* first command of the main is an auxilliary pass */
    tCommand *aux_pass=ifj_malloc(sizeof(tCommand),PERSISTENT);
    aux_pass->type = COMM_PASS;

    /* now we establish the main function record */
    tFunc *main_func=ifj_malloc(sizeof(tFunc),PERSISTENT);
    main_func->commands = aux_pass;
    main_func->paramnumber = 0;

    /* and set the start point to this function */
    start_point = main_func;

//printf("parsing begins now!\n");

    result = mainItemList();

    main_func->vartable_size = main_tsp;

    if(!result)
    {
        exit(SYNTAX_ERROR);
    }
//printf("syntax OK\n");

    if(!checkVariableNames())
    {
        exit(MISC_SEMANTIC_ERROR);
    }
//printf("var name X func name OK\n");

    if(!checkFunctionCalls())
    {
        exit(UNDEFINED_FUNC);
    }
//printf("function calls OK\n");

    if(!checkUndefSymbols())
    {
        exit(UNDEFINED_VAR);
    }
//printf("no undef symbols OK\n");

    if(!checkExpressionTrees())
    {
        exit(MISC_SEMANTIC_ERROR);
    }
//printf("types OK\n");

    if(!checkFuncNames())
    {
        exit(MISC_SEMANTIC_ERROR);
    }
//printf("function names oK\n");

    return 1;
}
