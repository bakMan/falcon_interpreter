/**
 * @FILE structures.h
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Contains data structures for the interpret
 * @NOTE Written to avoid circular dependencies in header files
 */

#ifndef _STRUCTURES_H_INCLUDED_
#define _STRUCTURES_H_INCLUDED_

#include "global.h"


/* **************************************** *
 ~ ~~~[ Typedefs ] ~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * **************************************** */

/** Expression types */
typedef enum {
    T_LITERAL,
    T_VARIABLE,
    T_PLUS,
    T_MINUS,
    T_ASTERISK,
    T_SLASH,
    T_POWER,
    T_EQ,
    T_NE,
    T_LT,   // less than
    T_GT,   // greater than
    T_LE,   // less or equal than
    T_GE,   // greater or equal than
    T_AND,
    T_OR,
    T_NOT,
    T_IN,
    T_NOTIN,
    T_FUNCCALL,
    T_INTFUNC,  // calling internal functions
    T_UMINUS,
} exprType;

/** Command types */
typedef enum {
    COMM_ASSIGNMENT,
    COMM_IFELS,
    COMM_RETURN,
    COMM_PASS,
} commandType;

typedef struct _function_ tFunc;
typedef struct _expression_ tExpr;
typedef struct _command_ tCommand;

/* ************************************ *
 ~ ~~~[ DATA STRUCTURES ]~~~~~~~~~~~~~~ ~
 * ************************************ */

struct _expression_ {
    exprType type;
    union {
        variable value;
        unsigned index; //!< variable table or internal functions table index
        tFunc * function;
        tExpr * children[2];
    } data;
    /** For real parameters' expression list */
    tExpr * params;
    tExpr * next;
};

/**
 * A command data structure
 * Contents specifications are in interpret/input_defs
 */
struct _command_ {
    commandType type;
    /* Variable (or built-in function) table index */
    unsigned index;
    tExpr *expression;
    tExpr *lvalue;
    /** Next command to be carried out */
    tCommand * next;
    /** Command to be carried out when condition is false in 'IFELS' */
    tCommand * alternative;
};

/**
 * Function table item
 * Contains the commandList to be executed, the number of formal parameters and 
 * size of the variable table
 */
struct _function_ {
    /** The list of commands to be executed */
    tCommand * commands;
    /** Number of function's formal parameters */
    unsigned paramnumber;
    /** Size of function's variable table */
    unsigned vartable_size;
};

#endif // _STRUCTURES_H_INCLUDED
