/**
 * @FILE vartable.h
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Contains data structures and prototypes of functions 
 * for the variable table
 * @VERSION 0.3
 */

#ifndef _VARTABLE_H_INCLUDED_
#define _VARTABLE_H_INCLUDED_

#include "global.h"
#include "structures.h"
#include "memory_interpret.h"

/**
 * Creates a variable table and returns pointer to it
 * @RETURN Pointer to the allocated variable table
 * @PARAM vartable_size Size of the variable table
 */
variable * createVartable(unsigned vartable_size);

/**
 * Fills the proper field of variable table with parameter values
 * @RETURN void
 * @PARAM vartable The variable table to fill
 * @PARAM paramnumber The number of function's formal parameters
 * @PARAM param The tExpr list representing parameters
 * @NOTE Could return, whether there were enough parameters, but I do not consider
 * this necessary
 */
void insertParams(variable * vartable, unsigned paramnumber, tExpr * param, variable * oldvartable);

/** Creates a copy of a variable
 *
 * For undefined, nil, boolean or numeric, this doesn't do anything
 * For other variables, a deep copy is created and returned
 * @PARAM[in] original Original variable to be copied
 * @RETURN variable The variable copy
 */
variable copyVariable (variable original, TE_alloc_type type);

/** Returns the 'truthness' of variable
 *
 * Returns, whetherthe variable evaluates as 'true' or 'false'.
 * Specifications may be found in project definition.
 * Invokes an error with variable of type differing from specifications
 * @RETURN bool The variable's 'truthness' value
 * @PARAM predicate The variable, which is evaluated
 */
bool isTrue(variable predicate);

/** Compares the variables and determines, which one is 'bigger'
 *
 * Only compares variables of the same type, should be checked before use
 * @PARAM[in] left Variable to be compared
 * @PARAM[in] right Variable to be compared
 * @RETURN int The number specifying, which variable is 'bigger'
 * Result == 0 ==> The variables equal
 * Result  < 0 ==> The left variable is greater than the right
 * Result  > 0 ==> The right variable is greater than the left
 */
int cmpVariables(variable left, variable right);

/** Destroy given variable
 * 
 * Deallocates (frees) necessary elements of variable
 * Calls itself recursively for destroying arrays
 * @PARAM current Variable to be destroyed
 */
void destroyVariable(variable *current);


#endif // _VARTABLE_H_INCLUDED
