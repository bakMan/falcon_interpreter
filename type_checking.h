/**
 * @file type_checking.h
 * @author Karel Benes
 */
#ifndef _TYPE_CHECKING_H_
#define _TYPE_CHECKING_H_

#include "structures.h"

/** Staticly checks types in the expression
 * 
 * @param expression pointer to the expression tree to check
 * @param line number of line, where the expression was written
 * @returns boolean whether the expression was found valid
 */ 
bool expressionValid(tExpr *expression, unsigned line);

#endif
