/**
 * @file ial.h
 * @author Karel Benes
 * @brief Shell sorting and subtring seaching header.
 */

#ifndef _IAL_H_
#define _IAL_H_

/** Shell sort implementation.
 *
 * Results in ascending sequence, starts with len/2 gap, then shortens it
 * to gap/2.2.
 *
 * @param[in,out] str pointer to string to be sorted
 * @param[in] len length of the string
 * @note Doesn't check either the lenght or "stringness" of the string so
 * can be used for any nasty tricks.
 */
void sort(char *str, int len);

/** Finding the first occurance of a substring.
 *
 *@param[in] str the string to be searched for a pattern
 *@param[in] pattern the pattern to be found
 *@return index denoting beginning the first pattern occurance in the string
 *@return in case the pattern is not found, returns -1
 */
int substringPosition(char *str,char *pattern);
#endif
