# Project: IFJ12
# File: Makefile
# Authors:
# - Stanislav Smatana (xsmata01)
# - Dagmar Prokopova (xproko26)
# - Karel Benes (xbenes20)
# Faculty: FIT VUT BRNO

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

all:
	make IFJ12

main.o: main.c scanner.h global.h error.h parser.h structures.h interpret.h tree_debug.h
	$(CC) $(CFLAGS) -c main.c -o $@

scanner.o: scanner.c scanner.h global.h error.h
	$(CC) $(CFLAGS) -c scanner.c -o $@

error.o: error.c error.h
	$(CC) $(CFLAGS) -c error.c -o $@

parser.o: parser.c parser.h structures.h global.h scanner.h bst.h memory.h prec_rules.arr
	$(CC) $(CFLAGS) -c parser.c -o $@

type_checking.o: type_checking.c type_checking.h structures.h global.h 
	$(CC) $(CFLAGS) -c type_checking.c -o $@

bst.o: bst.c bst.h memory.h
	$(CC) $(CFLAGS) -c bst.c -o $@

tree_debug.o: tree_debug.c tree_debug.h structures.h
	$(CC) $(CFLAGS) -c tree_debug.c -o $@

memory.o: memory.c memory.h bst.h error.h global.h parser.h scanner.h structures.h memory_main.h memory_scanner.h memory_parser.h memory_optimizer.h memory_interpret.h
	$(CC) $(CFLAGS) -c memory.c -o $@

variable.o: variable.c variable.h global.h structures.h memory.h interpret.h
	$(CC) $(CFLAGS) -c variable.c -o $@

functions.o: functions.c functions.h structures.h global.h interpret.h variable.h ial.h
	$(CC) $(CFLAGS) -c functions.c -o $@ -lm

interpret.o: interpret.c interpret.h global.h structures.h variable.h functions.h
	$(CC) $(CFLAGS) -lm -c interpret.c -o $@

ial.o: ial.c ial.h memory.h global.h
	$(CC) $(CFLAGS) -c ial.c -o $@

generic.o: generic.c global.h structures.h interpret.h memory.h memory_interpret.h
	$(CC) $(CFLAGS) -c generic.c -o $@

IFJ12: main.o scanner.o bst.o tree_debug.o parser.o error.o interpret.o functions.o variable.o memory.o ial.o type_checking.o generic.o
	$(CC) $(CFLAGS) $^ -lm -o $@

interpret:
	$(MAKE) -C ./interpret

memory: error.o memory.c memory.h 
	$(CC) $(CFLAGS) memory.c error.o -DMEMORY_DEBUG -o memory_build

clean:
	rm -rf build IFJ12 main.o scanner.o bst.o tree_debug.o parser.o error.o interpret.o functions.o variable.o memory.o ial.o type_checking.o generic.o memory_build core

ship: doc
	sed -r -i -e 's/(^CFLAGS=.*)(-g)/\1-O2 -DNDEBUG/' Makefile
	zip xbenes20 *.c *.h *.arr Makefile dokumentace.pdf rozdeleni rozsireni

.PHONY: doc interpret ship test

doc:
	doxygen doc/doxygen.conf
	$(MAKE) -C doc/latex
	cp doc/latex/interpret.pdf dokumentace.pdf

test: all
	test_suite/runtests.sh -t test_suite/sanity
	test_suite/runtests.sh -t test_suite/optimization
	test_suite/runtests.sh -t test_suite/merlin

clean-test:
	test_suite/runtests.sh -c test_suite/sanity
	test_suite/runtests.sh -c test_suite/optimization
	test_suite/runtests.sh -c test_suite/merlin

