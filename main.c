/**
 * @file main.c
 * @brief Initial file containing function main.
 * @author Dagmar Prokopova (xproko26)
 * @edit   David Kaspar (xkaspa34) aka Dee'Kej
 */

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "global.h"
#include "parser.h"
#include "structures.h"
#include "interpret.h"
#include "tree_debug.h"
#include "memory_main.h"


/**
 * Closes the source file. This is just wrapper function so it can be called
 * with atexit() function.
 */
void close_source(void)
{
    fclose(sourcefile);
    return;
}


/** Checks params, opens src. file, passes control to syntax analyzator.
 *
 *@param[in] argc number of input parameters
 *@param[in] argv storage of input parameters
 *@return exitcode according to the specification
 */
int main(int argc, char *argv[])
{
    //initialization
    exitcode = OK;

    //check of params
    if (argc != 2)
    {
        printError("Wrong number of arguments.");
        return INTERNAL_ERROR;
    }

    //open the source file
    if ((sourcefile = fopen(argv[1], "r")) == NULL)
    {
        printError("Cannot open file '%s'.", argv[1]);
        return INTERNAL_ERROR;
    }

    // making sure the file will be closed whatever happens
    atexit(close_source);

 // // // // // // // // // // // // // // // // // // // // // // // // // //
    
    // preparing memory manager for 1st run
    memory_setup(PARSER);

    // parsing source code and proceeding if everything went fine
	  if(parseSource())
    {

#ifdef OPTIMIZER_ON
        // preparing memory manager for next run stage
        memory_setup(OPTIMIZER);

        // FIXME: HERE SHOULD BE OPTIMIZER FUNCTION CALL !!!
#endif

#ifndef NDEDUG
        // for debugging purposes only
        dumpProgramGraph("prog.dot",start_point);
#endif

        // preparing memory manager for last run stage
        memory_setup(INTERPRET);
        
        // executing from start_point == interpreting the given program code
        executeFunction(start_point,NULL,NULL);
    }

    return exitcode;
}
