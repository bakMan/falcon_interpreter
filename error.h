/**
 * @file error.h
 * @brief Interface of module for error printing.
 * @author Dagmar Prokopova (xproko26)
 * @edited David Kaspar (xkaspa34)
 */

#ifndef _ERROR_H_INCLUDED_
#define _ERROR_H_INCLUDED_


/**
 * Return values (error codes) of ifj12 compiler/interpreter.
 */
enum return_codes {
    OK = 0,                           /* Everything went fine. */
    LEXICAL_ERROR = 1,                /* Lexical analysis error. */
    SYNTAX_ERROR = 2,                 /* Syntactic analysis error. */
    UNDEFINED_VAR = 3,                /* Semantic error - undefined variable. */
    UNDEFINED_FUNC = 4,               /* Semantic error - undefined function. */
    MISC_SEMANTIC_ERROR = 5,          /* Other semantic errors. */
    ZERO_DIVISION = 10,               /* Runtime error - division by zero. */
    TYPE_INCOMPATIBILITY = 11,        /* Runtime error - incompatible types. */
    NUMERIC_ERROR = 12,               /* Runtime error of typecasting. */
    MISC_RUNTIME_ERROR = 13,          /* Other runtime errors. */
    ERR_42 = 42,                      /* Assertion error - internal only. */
    INTERNAL_ERROR = 99,              /* Internal error. */
};


/** Function with variable number of arguments for error printing.
 *
 * @param[in] fmt string containing formating signs
 */
void printError(const char* fmt, ...);

#endif  /* End of _ERROR_H_INCLUDED_ */

