/**
 * @file global_constants.h
 * @author Karel Benes (xbenes20)
 * @edited David Kaspar (xkaspa34)
 * @brief Holds constants used throughout whole program.
 * @version 0.2
 *
 * - Return codes according to the specification.
 * .
 */


#ifndef _GLOBAL_H_INCLUDED_
#define _GLOBAL_H_INCLUDED_

#include <stdbool.h>
#include <stdio.h>

#include "error.h"                /* Including due to backward compatibility. */

/** Macros */
#define left_child data.children[0]
#define right_child data.children[1]

#define dbgError(EXP, ...) \
/*
    do { \
        fprintf(stderr, "%s in %s: ", __func__, __FILE__); \
        fprintf(stderr, EXP, __VA_ARGS__); fprintf(stderr, "\n"); \
        exit(ERR_42); \
    } while (0)
*/

/* END of Macros */


/** Internal functions enum
 * Either the internal functions can be added to the function table or
 * have their own separate calling. I chose the latter option
 */
/** Warning! Whoever dares to add/remove a function here, HAS to go
 * to the file 'parser.c' and fix the difinition of the array 
 * 'const char *builtin_func_ident[]'! He who doesn't will be welcome
 * to debug such a nice SIGSEGV arising from strcmp().
 */
typedef enum {
    FUNC_PRINT,
    FUNC_INPUT,
    FUNC_NUMERIC,
    FUNC_TYPEOF,
    FUNC_LEN,
    FUNC_FIND,
    FUNC_SORT,
    FUNC_ARRAYINIT,
    FUNC_SUBVAR,
    FUNC_ACCESS,
    FUNC_NOT_BUILTIN
} intFunction;

/** Variable types
 * An enum of variable types to be used in the variable data structure
 */
typedef enum {
    VAR_UNDEFINED,
    VAR_NIL,
    VAR_BOOL,
    VAR_NUMERIC,
    VAR_STRING,
    VAR_ARRAY,
    VAR_FUNCNAME,
    VAR_UNKNOWN,
}variable_type;

/** Variable representation structure
 * This describes a structre we will use to represent variables in IFJ12
 * Each variable consists of its type and value.
 * @NOTE Creating 'string' and 'array' variables requires memory allocation
 */
typedef struct ifj12variable variable;

struct ifj12variable {
    variable_type type;
    bool no_touch;
    union _value_ {
        bool boolean;
        double numeric;
        /**
         * String is a structure containing also its length
         */
        struct {
            unsigned length;
            char * data;
        } string;
        /**
         * Array is also a structure containing its size
         */
        struct {
            /** Count of array members, not malloc'd size */
            unsigned size;
            variable * data;
        } array;
    } value;
};

/* global variables */
FILE *sourcefile;
int exitcode;
void *start_point; /* this is passed on to the interpret
                     it is untyped to avoid cyclic dependence of header files */

#endif // _GLOBAL_H_INCLUDED_
