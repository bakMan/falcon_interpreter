/**
 * @file      memory_optimizer.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file for OPTIMIZER module of memory management for IFJ12
 *            interpret.
 *
 * @detailed  This header file contains specific interface for OPTIMIZER only.
 *            Interface common for every module is defined in memory.h file.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY_OPTIMIZER.H ]**************************************** *
 * ************************************************************************** */

/* Only if OPTIMIZER is enabled. */
#ifdef OPTIMIZER_ON

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_OPTIMIZER_H
#define MEMORY_OPTIMIZER_H


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include "memory.h"                           /* Common interface. */


/* ************************************************************************** *
 * ***[ END OF MEMORY_OPTIMIZER.H ]****************************************** *
 * ************************************************************************** */

#endif
#endif

