/**
 * @file   tree_debug.h
 * @brief  Functions for graphical syntactic tree dumping
 * @author Stanislav Smatana (xsmata01) 
 */

#ifndef _TREE_DEBUG_H
#define _TREE_DEBUG_H

/** Function to dump abstract syntax graph into graphviz dot script.
 * 
 * @param out_file         Filename of the file to dump graph into. If NULL
 *                         stdout will be used.
 *                         
 * @param root_function    Pointer to root function of abstract syntax tree.
 * 
 * @bug For now, '\n' chars in falcon source lead to incorrect dot source.
 */
bool dumpProgramGraph(const char* out_file,void *root_function);

#endif
