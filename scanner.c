/**
 * @file scanner.c
 * @brief Module for scanner implementation.
 * @author Dagmar Prokopova (xproko26)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

#include "scanner.h"
#include "global.h"
#include "error.h"
#include "memory_scanner.h"
#include "memory.h"


/* States for FSM implementation of lexical analyser used only inside of this module.
 * Redundant states are not included. Once the token is send to syntax analyser, FSM 
 * is switched to S_Start state by initialization at the very begining of getToken().
 */
typedef enum fsm_state {
    S_Start,             // initial state
    S_EOF,               // end of file
    S_Slash,             // '/' loaded
    S_CommLine,          // crunching one-line-comment
    S_CommBlock,         // crunching block-comment
    S_CommBlockEnd,      // crunching block-comment, '*' loaded, '/' awaited
    S_Ident,             // identifier
    S_String,            // string
    S_WrapString,        // wrapped string
    S_WrapId,            // wrapped identifier
    S_BracketedWrapId,   // wrapped identifier in brackets
    S_BrackWrapIdEnd,    // waiting for the end '(' of bracketed wrapped identifier
    S_Escape,            // escape sequence inside of string
    S_EscDig1,           // escape sequence awaiting first hexa digit
    S_EscDig2,           // escape sequence awaiting second hexa digit
    S_NumInt,	           // numbers-integer loaded
    S_NumFrac,           // numbers-fraction, '.' loaded
    S_NumFracNum,        // numbers-fraction, '.' and any digit loaded
    S_NumExp,            // numbers-exponent, 'e' loaded, sign or number awaited
    S_NumExpNum,         // numbers-exponent, 'e'||'e+'||'e-' and any digit loaded
    S_NumExpSign,        // numbers-exponent, 'e+' or 'e-' loaded, number awaited
    S_Asterisk,          // '*' loaded
    S_Equate,            // '=' loaded
    S_Exclam,            // '!' loaded
    S_Less,              // '<' loaded
    S_Greater,           // '>' loaded
    } TState;


//-----------------------------------------------------------------------------



/** Allocates an array of initial BUFFER_LENGTH 16 chars.
 *
 * @param[in] bufsize size of buffer to be allocated (16)
 * @param[in] type type of buffer to be allocated
 * @return pointer to newly created buffer
 */
char* createBuffer(unsigned long *bufsize, TE_alloc_type type)
{
   *bufsize = BUFFER_LENGTH;
   char *ptr = ifj_malloc((*bufsize)*sizeof(char), type);
   return ptr;
}


/** Appends the char into the buffer (on the first "empty" place).
 *
 * When there is not enough space for the new char, it reallocates the buffer.
 * @param[in,out] buffer pointer to array of chars
 * @param[in,out] bufsize actual size of buffer
 * @param[in] type type of buffer which it is worked with
 * @param[in] oldindex position where the previous char was placed in
 * @param[in] c char to be appended
*/
void bufAppend(char **buffer, unsigned long *bufsize, TE_alloc_type type, unsigned long *oldindex, char c)
{

    //check whether there is enough space for actual char and '\0' in the buffer
    if ((*oldindex) > ((*bufsize)-2))
    {
        *bufsize = (*bufsize)+BUFFER_LENGTH;
        *buffer = ifj_realloc(*buffer, (*bufsize)*sizeof(char), type);
    }

    (*buffer)[*oldindex] = c;
    (*oldindex)++;

}



//-----------------------------------------------------------------------------


/* array of keywords, indexes corresponding to token types */
const char* keyword[] =
{
    [TKW_and] =         "and",
    [TKW_elif] =        "elif",
    [TKW_else] =        "else",
    [TKW_end] =         "end",
    [TKW_false] =       "false",
    [TKW_for] =         "for",
    [TKW_function] =    "function",
    [TKW_if] =          "if",
    [TKW_in] =          "in",
    [TKW_loop] =        "loop",
    [TKW_nil] =         "nil",
    [TKW_not] =         "not",
    [TKW_notin] =       "notin",
    [TKW_or] =          "or",
    [TKW_return] =      "return",
    [TKW_true] =        "true",
    [TKW_while] =       "while",
};


/* array of reserved words */
const char* resword[] =
{
    "as",
    "def",
    "directive",
    "export",
    "from",
    "import",
    "launch",
    "load",
    "macro",
};


/** Checks the key- reserved- words and returns the appropriate token type (int). 
 *
 * @param[in] identifier string representing identifier, keyword or reserved word
 * @return integer (token type) representing the key- reserved- word or simple identifier
 */
int checkKeyResWords(char* identifier)
{
    for (int i = 0; i < RES_WORDS; i++)
    {
        if (strcmp(identifier, resword[i])==0) return TReserveWord;
    }

    if (strcmp(identifier, keyword[TKW_and])==0) return TKW_and;
    else if (strcmp(identifier, keyword[TKW_elif])==0) return TKW_elif;
    else if (strcmp(identifier, keyword[TKW_else])==0) return TKW_else;
    else if (strcmp(identifier, keyword[TKW_end])==0) return TKW_end;
    else if (strcmp(identifier, keyword[TKW_false])==0) return TKW_false;
    else if (strcmp(identifier, keyword[TKW_for])==0) return TKW_for;
    else if (strcmp(identifier, keyword[TKW_function])==0) return TKW_function;
    else if (strcmp(identifier, keyword[TKW_if])==0) return TKW_if;
    else if (strcmp(identifier, keyword[TKW_in])==0) return TKW_in;
    else if (strcmp(identifier, keyword[TKW_loop])==0) return TKW_loop;
    else if (strcmp(identifier, keyword[TKW_nil])==0) return TKW_nil;
    else if (strcmp(identifier, keyword[TKW_not])==0) return TKW_not;
    else if (strcmp(identifier, keyword[TKW_notin])==0) return TKW_notin;
    else if (strcmp(identifier, keyword[TKW_or])==0) return TKW_or;
    else if (strcmp(identifier, keyword[TKW_return])==0) return TKW_return;
    else if (strcmp(identifier, keyword[TKW_true])==0) return TKW_true;
    else if (strcmp(identifier, keyword[TKW_while])==0) return TKW_while;
    else return TIdentifier;
}


/** Transformes char representing hexadecimal digit to decimal value.
 *
 * @param[in] chr hexa-char: one of [1..9]|[a..f]
 * @return integer value
 */
int hexchToDec(char chr)
{
    if (isdigit(chr))
        return ((int)chr)-(int)'0';
    else
        return ((int)toupper(chr))-(int)'A'+10;
}


/** Analyses the source file and returns one token. 
 *
 * Pointer to a source file is a global variable (see global.h), therefore no input
 * parameters are needed. Function is implemented as the finite state machine.
 * It returns token - structure containing token type, position in the source file
 * (line and col). If toknen type is Numeric Literal it contains also the value (double),
 * for Identifier|Keyword|Resword|StringLiteral it contains pointer to buffer where the
 * string is stored. When invalid token is found, it eats the rest of the line till the EOL
 * is found.
 *
 * @return structure containing token.type, [value (num or string)], position
 */
TToken getToken()
{
    /*initialization*/
    TState state = S_Start;  //state of FSM
    bool complete = false;  //true when whole token loaded

    static bool wrapstr1 = false;  //true when @ was loaded, wrapstr awaited
    static bool wrapstr2 = false;  //true when @ and initial " loaded, wrapstr/wrapid awaited

    static bool unminus = true;  //false when Identifier or NumLiteral loaded

    static unsigned int line = 1;  //line counter
    static unsigned int col = 0;  //column counter

    char *buffer = NULL;  //pointer to actual buffer
    unsigned long bufsize = 0;  //actual size of buffer if created

    static char *numbuffer = NULL;  //pointer to numeric buffer
    static unsigned long numbufsize = 0;  //actual size of numeric buffer if created

    unsigned long charcounter = 0;  //length of string/identifier/numliteral

    int escval = 0;  //storage for the first digit of escape sequence


    TToken token =
    {
        .type = TUndefined,
        .length = 0,
        .line = 0,
        .col = 0,
    };

    //finite state machine
    while (!complete)
    {

        // create numeric buffer when getToken called for the first time
        if (numbuffer == NULL)
            numbuffer = createBuffer(&numbufsize, BUFFER);

        int c = fgetc(sourcefile);
        col++;

        switch (state)
        {
            case S_Start:
            {
                if (wrapstr2)
                {
                    if (c == '$')
                    {
                        state = S_WrapId;
                        buffer = createBuffer(&bufsize, IDENTIFIER);
                    }
                    else if (c == '"')
                    {   
                        ;  // stay in the S_Start state
                        wrapstr1 = false;
                        wrapstr2 = false;
                    }
                    else
                    {
                        state = S_WrapString;
                        buffer = createBuffer(&bufsize, LITERAL);
                        ungetc(c, sourcefile);
                        col--;
                    }
                }
                else if (c == EOF)
                {
 		            complete = true;  // we get 'EOF' here
                    token.type = TEOF;
                }
                else if (c == '\n')
                {
                    complete = true;  // we get 'EOL' here
                    token.type = TEOL;
                }
		        else if (c == '/')
                {
                    state = S_Slash;
                }
                else if (c == '+')
                {
                    complete = true;  // we get '+' here
                    token.type = TOpPlus;
                }
                else if (c == '-')
                {
                    complete = true;  // we get '-' here
                    if (unminus)
                    {
                        token.type = TOpUnMinus;
                    }
                    else
                    {
                        token.type = TOpMinus;
                    }
                }
                else if (c == '*')
                {
                    state = S_Asterisk;
                }
                else if (c == '=')
                {
                    state = S_Equate;
                }
                else if (c == '!')
                {
                    state = S_Exclam;
                }
                else if (c == '<')
                {
                    state = S_Less;
                }
                else if (c == '>')
                {
                    state = S_Greater;
                }
                else if (c == '(')
                {
                    complete = true;  // we get '(' here
                    token.type = TLeftParenthesis;
                }
                else if (c == ')')
                {
                    complete = true;  // we get ')' here
                    token.type = TRightParenthesis;
                }
                else if (c == '[')
                {
                    complete = true;  // we get '[' here
                    token.type = TLeftBracketSquare;
                }
                else if (c == ']')
                {
                    complete = true;  // we get ']' here
                    token.type = TRightBracketSquare;
                }
                else if (c == ',')
                {
                    complete = true;  // we get ',' here
                    token.type = TComma;
                }
                else if (c == ':')
                {
                    complete = true;  // we get ':' here
                    token.type = TColon;
                }
                else if (c == '@')
                {
                    complete = true;  // we get '@' here
                    token.type = TOpAt;
                    wrapstr1 = true;
                }
                else if (c == '"')
                {
                    if (wrapstr1 == true)
                    {
                        wrapstr2 = true;
                        state = S_WrapString;
                    }
                    else
                    {
                        state = S_String;
                    }
                    buffer = createBuffer(&bufsize, LITERAL);
                }
                else if ((c == '_') || (isalpha(c)))
                {
                    state = S_Ident;
                    buffer = createBuffer(&bufsize, IDENTIFIER);
                    bufAppend(&buffer, &bufsize, IDENTIFIER, &charcounter, c);
                }
                else if (isdigit(c))
                {
                    state = S_NumInt;
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                }
                else if (isspace(c))
                {
                    ; // stay in the initial state
                }
                else
                {
                    // error: symbol not in alphabet
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u unrecognised symbol", line, col);
                    exit(exitcode);
                }

                token.line = line;
                token.col = col;
            } break;

            case S_Slash:
            {
                if (c == '/')
                {
                    state = S_CommLine;
                }
                else if (c == '*')
                {
                    state = S_CommBlock;
                }
                else
                {
                    complete = true;  // we get '/' here
                    token.type = TOpDivider;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_Asterisk:
            {
                if (c == '*')
                {
                    complete = true;  // we get '**' here
                    token.type = TOpPower;
                }
                else
                {
                    complete = true;  // we get '*' here
                    token.type = TOpMultiplicator;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_Equate:
            {
                if (c == '=')
                {
                    complete = true;  // we get '==' here
                    token.type = TOpEqual;
                }
                else
                {
                    complete = true;  // we get '=' here
                    token.type = TAssignment;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_Exclam:
            {
                if (c == '=')
                {
                    complete = true;  // we get '!=' here
                    token.type = TOpNotEqual;
                }
                else
                {
                    complete = true;
                    token.type = TInvalid;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_Less:
            {
                if (c == '=')
                {
                    complete = true;  // we get '<=' here
                    token.type = TOpLessEqual;
                }
                else
                {
                    complete = true;  // we get '<' here
                    token.type = TOpLess;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_Greater:
            {
                if (c == '=')
                {
                    complete = true;  // we get '>=' here
                    token.type = TOpGreaterEqual;
                }
                else
                {
                    complete = true;  // we get '>' here
                    token.type = TOpGreater;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_CommLine:
            {
                if ((c == '\n') || (c == EOF))
                {
                    ungetc(c, sourcefile);
                    col--;
                    state = S_Start;
                }
                else
                {
                    ;  // stay in the S_CommLine state
                }
            } break;

            case S_CommBlock:
            {
                if (c == '*')
                {
                    state = S_CommBlockEnd;
                }
                else if (c == '\n')
                {
                    ;  // stay in the S_CommBlock state
                    line++;
                    col = 0;
                }
                else if (c == EOF)
                {
                    // error: unfinished block comment
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u the end of block comment not found", line, col);
                    exit(exitcode);
                }
                else
                {
                    ;  // stay in the S_CommBlock state
                }
            } break;

            case S_CommBlockEnd:
            {
                if (c == '/')
                {
                    state = S_Start;
                }
                else
                {
                    state = S_CommBlock;  // one state back, wait for '*' first
                }
            } break;

            case S_String:
            {
                if (c == '"')
                {
                    complete = true;
                    buffer[charcounter] = '\0';
                    token.type = TLiteralStr;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                }
                else if (c == '\\')
                {
                    state = S_Escape;
                }
                else if (isprint(c))
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, c);
                    ;  // stay in the S_String state;
                }
                else
                {
                    // error: symbol not printable
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u untypical symbol in string literal", line, col);
                    exit(exitcode);
                }
            } break;

            case S_WrapString:
            {
                if (c == '"')
                {
                    complete = true;
                    wrapstr1 = false;
                    wrapstr2 = false;
                    buffer[charcounter] = '\0';
                    token.type = TWrapString;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                }
                else if (c == '\\')
                {
                    state = S_Escape;
                }
                else if (c == '$')
                {
                    complete = true;
                    buffer[charcounter] = '\0';
                    token.type = TWrapString;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                    ungetc(c, sourcefile);
                    col--;
                }
                else if (isprint(c))
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, c);
                    ;  // stay in the S_WrapString state;
                }
                else
                {
                    // error: symbol not printable
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u untypical symbol in string literal", line, col);
                    exit(exitcode);
                }
            } break;

            case S_WrapId:
            {
                if (c == '(')
                {
                    state = S_BracketedWrapId;
                    ;
                }
                else if ((isalpha(c)) || (isdigit(c)) || (c == '_'))
                {
                    bufAppend(&buffer, &bufsize, IDENTIFIER, &charcounter, c);
                    ;  // stay in the S_WrapId state
                }
                else
                {
                    complete = true;
                    buffer[charcounter] = '\0';
                    token.type = TWrapIdent;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                    ungetc(c, sourcefile);
                    col--;
                }
            } break;

            case S_BracketedWrapId:
            {
                if ((isalpha(c)) || (isdigit(c)) || (c == '_'))
                {
                    bufAppend(&buffer, &bufsize, IDENTIFIER, &charcounter, c);
                    ;  // stay in the S_BracketedWrapId state
                }
                else if (c == ' ')
                {
                    if (charcounter > 0)
                        state = S_BrackWrapIdEnd;
                }
                else if (c == ')')
                {
                    complete = true;
                    buffer[charcounter] = '\0';
                    token.type = TWrapIdent;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                }
                else
                {
                    // error: symbol not permissed in identifier
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u illegal symbol in identifier construction", line, col);
                    exit(exitcode);
                }
            } break;

            case S_BrackWrapIdEnd:
            {
                if (c == ' ')
                {
                    ;  //stay in the S_BrackWrapIdEnd state
                }
                else if (c == ')')
                {
                    complete = true;
                    buffer[charcounter] = '\0';
                    token.type = TWrapIdent;
                    token.value.ptr = buffer;
                    token.length = charcounter;
                }
                else
                {
                    // error: not finished bracketed identifier
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u expected ')' instead of '%c'", line, col, c);
                    exit(exitcode);
                }
            } break;

            case S_Escape:
            {
                if (c == 'n')
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, '\n');
                    if (wrapstr1 == true)
                        state = S_WrapString;
                    else
                        state = S_String;
                }
                else if (c == 't')
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, '\t');
                    if (wrapstr1 == true)
                        state = S_WrapString;
                    else
                        state = S_String;
                }
                else if (c == '\\')
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, '\\');
                    if (wrapstr1 == true)
                        state = S_WrapString;
                    else
                        state = S_String;
                }
                else if (c == '"')
                {
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, '\"');
                    if (wrapstr1 == true)
                        state = S_WrapString;
                    else
                        state = S_String;
                }
                else if (c == 'x')
                {
                    state = S_EscDig1;
                }
                else
                {
                    // error: invalid escape sequence
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u invalid escape sequence", line, col);
                    exit(exitcode);
                }
            } break;

            case S_EscDig1:
            {
                if (isxdigit(c))
                {
                    escval = hexchToDec(c);
                    state = S_EscDig2;
                }
                else
                {
                    // error: not a hexa digit
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u hexadecimal digit expected", line, col);
                    exit(exitcode);
                }
            } break;

            case S_EscDig2:
            {
                if (isxdigit(c))
                {
                    escval = (16*escval) + hexchToDec(c);
                    bufAppend(&buffer, &bufsize, LITERAL, &charcounter, (char)escval);
                    if (wrapstr1 == true)
                        state = S_WrapString;
                    else
                        state = S_String;
                }
                else
                {
                    // error: not a hexa digit
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u hexadecimal digit expected", line, col);
                    exit(exitcode);
                }
            } break;

            case S_Ident:
            {
                if ((isalpha(c)) || (isdigit(c)) || (c == '_'))
                {
                    bufAppend(&buffer, &bufsize, IDENTIFIER, &charcounter, c);
                    ;  // stay in the S_Ident state
                }
                else
                {
                    complete = true;
                    ungetc(c, sourcefile);
                    col--;
                    buffer[charcounter] = '\0';
                    token.type = checkKeyResWords(buffer);
                    token.value.ptr = buffer;
                    token.length = charcounter;
                    unminus = false;
                }
            } break;

            case S_NumInt:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    ;  // stay in the S_NumInt state
                }
                else if (c == '.')
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumFrac;
                }
                else if (c == 'e')
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumExp;
                }
                else
                {
                   // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
            } break;

            case S_NumFrac:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumFracNum;
                }
                else
                {
                    // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
            } break;

            case S_NumFracNum:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    ; // stay in S_NumFracNum state
                }
                else if (c == 'e')
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumExp;
                }
                else if ((c == '.') || (isalpha(c)))
                {
                    // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
                else
                {
                    complete = true;
                    ungetc(c, sourcefile);
                    col--;
                    numbuffer[charcounter] = '\0';
                    token.type = TLiteralNum;
                    token.value.num = strtod(numbuffer, NULL);
                    unminus = false;
                }
            } break;

            case S_NumExp:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumExpNum;
                }
                else if ((c == '+') || (c == '-'))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumExpSign;
                }
                else
                {
                    // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
            } break;

            case S_NumExpSign:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    state = S_NumExpNum;
                }
                else
                {
                    // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
            } break;

            case S_NumExpNum:
            {
                if (isdigit(c))
                {
                    bufAppend(&numbuffer, &numbufsize, BUFFER, &charcounter, c);
                    ; // stay in the S_NumExpNum state
                }
                else if ((c == '.') || (isalpha(c)))
                {
                    // error: illegal numeric literal
                    exitcode = LEXICAL_ERROR;
                    printError("%u:%u numeric literal in wrong format", line, col);
                    exit(exitcode);
                }
                else
                {
                    complete = true;
                    ungetc(c, sourcefile);
                    col--;
                    numbuffer[charcounter] = '\0';
                    token.type = TLiteralNum;
                    token.value.num = strtod(numbuffer, NULL);
                    unminus = false;
                }
            } break;

            default:
            {
                //internal error, we should never get here
                exitcode = INTERNAL_ERROR;
                printError("scanner.c: invalid state of lexical analyser");
                exit(exitcode);
            }

        } // end of switch(state)


        if ((!unminus) && (token.type != TIdentifier) && (token.type != TInvalidNumInt) && 
        (token.type != TLiteralNum) && (token.type != TRightParenthesis) && (token.type != TUndefined))
            unminus = true;

        if ((c == '\n') && (token.type == TEOL))
        {
            line++;
            col = 0;
        }

    } // end of while(!complete)

    return token;
}



//*****************************************************************************
//*****************************************************************************


#ifdef TESTSCANNER
/** For testing purposes only.
 *
 * Requires TESTSCANNER defined for preprocessor, therefore is likely to
 * be compiled with -DTESTSCANNER.
 */
int main(int argc, char *argv[])
{
    exitcode = OK;
    if (argc != 2)
    {
        exitcode = INTERNAL_ERROR;
        printError("Wrong number of arguments.");
        return exitcode;
    }

    if ((sourcefile = fopen(argv[1], "r")) == NULL)
    {
        exitcode = INTERNAL_ERROR;
        printError("Cannot open file '%s'.", argv[1]);
        return exitcode;
    }

    //-------------------------------------------------------------------------


    TToken token = getToken();
    while (token.type != TEOF)
    {
        printf("token.type = %d; line = %u; col = %u; \n", token.type, token.line, token.col);
        if ((token.type == TLiteralStr) || (token.type == TIdentifier) || (token.type == TWrapString) || (token.type == TWrapIdent))
            printf("%s; delka: %lu\n", token.value.ptr, token.length);
        if ((token.type == TLiteralNum) || (token.type == TInvalidNumInt))
            printf("%lf\n", token.value.num);
        token = getToken();
    }

    //-------------------------------------------------------------------------


    fclose(sourcefile);
    return exitcode;
}
#endif


