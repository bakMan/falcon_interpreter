/**
 * @file ial.c
 * @author Karel Benes
 * @brief Shell sort and substring KMP search implementation.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "global.h"
#include "memory_interpret.h"
#include "ial.h"


/** Builds partial match table for the KMP algorithm.
 *
 *@param[in] pattern the string for which we build the table
 *@param[out] table storage for the partial match table
 *@param[in] pattern_len number of character of the pattern
 */
void buildKMPTable(char *pattern, int *table, int pattern_len)
{
    int cnd=0; /* candidate -- length of actually acceptable prefix */
    int i=2;
    table[0]=-1;
    table[1]=0;

    while(i<pattern_len)
    {
        if(pattern[i-1]==pattern[cnd])
        {
            cnd++;
            table[i] = cnd;
            i++;
        }
        else if(cnd>0)
        {
            cnd = table[cnd];
        }
        else
        {
            table[i] = 0;
            i++;
        }
    }
}

/** Finding the first occurance of a substring.
 *
 *@param[in] str the string to be searched for a pattern
 *@param[in] pattern the pattern to be found
 *@return index denoting beginning the first pattern occurance in the string
 *@return in case the pattern is not found, returns -1
 */
int substringPosition(char *str,char *pattern)
{
    int pattern_len=strlen(pattern);
    int *table;

    /* empty string is at the beginning of any string */
    if(pattern_len==0)
    {
        return 0;
    }

    /* for one character, we need no table */
    if(pattern_len==1)
    {
        char c=pattern[0];
        for(int i=0;str[i]!='\0';i++)
        {
            if(str[i]==c)
            {
                return i;
            }
        }
        return -1;
    }

    /* for |pattern|>1, we first build "partial match" table */
    table = ifj_malloc(pattern_len*sizeof(int), ST_STACK);
    buildKMPTable(pattern,table, pattern_len);

    /* now we go to the searching itself */
    int match=0; /* current possible match beginning -- to be returned */
    int i=0; /* index of the currently checked */

    while(str[match+i]!='\0')
    {
        /* if the current character fits the pattern */
        if(pattern[i]==str[match+i])
        {
            /* if we are at the end of the pattern, return the match */
            if(i==pattern_len-1)
            {
                return match;
            }
            /* else move on */
            i++;
        }
        else
        {
            /* we skip the already-checked-string and roll back according
             to the partial match table*/
            match += i-table[i];
            if(table[i]>-1)
            {
                i = table[i];
            }
            else
            {
                i = 0;
            }
        }
    }

    /* in case we got here, the pattern couldn't be found */

    return -1;
}

/** Shell sort implementation.
 *
 * Results in ascending sequence, starts with len/2 gap, then shortens it
 * to gap/2.2.
 *
 * @param[in,out] str pointer to string to be sorted
 * @param[in] len length of the string
 * @note Doesn't check either the lenght or "stringness" of the string so
 * can be used for any nasty tricks.
 */
void sort(char *str, int len)
{
    /* Compare is a binary relation, thus we need at least two chars */
    if(len<2)
    {
        return;
    }

    int gap=len/2;
    do{
        for(int base_index=gap;base_index<len;base_index++)
        {
            char temp=str[base_index];
            int index;
            for(index=base_index;index>=gap;index-=gap)
            {
                if(temp >= str[index-gap])
                {
                    break;
                }
                str[index] = str[index-gap];
            }
            str[index] = temp;
        }

        if(gap>2)
        {
            gap=gap/2.2;
        }
        else
        {
            gap--;
        }
    }while(gap>0);

    return;
}

#ifdef TESTSEARCH
/** "Unit test", searches the second argument in the first
 *
 * Requires TESTSEARCH defined for preprocessor, therefore is likely to
 * be translated with -DTESTSEARCH.
 */
int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        printf("Exactly two arguments are mandatory\n");
    }
    else
    {
        printf("%d\n",substringPosition(argv[1],argv[2]));
    }
}
#endif

#ifdef TESTSORT
/** "Unit test", runs every argument from shell through the shell sort.
 *
 * Requires TESTSORT defined for preprocessor, therefore is likely to
 * be translated with -DTESTSORT.
 */
int main(int argc, char *argv[])
{
    if(argc == 1)
    {
        printf("Potrebuju parametry, ktere mam seradit\n");
    }
    else
    {
        for(int i=1;i<argc;i++)
        {
            sort(argv[i],strlen(argv[i]));
            puts(argv[i]);
        }
    }
}
#endif
