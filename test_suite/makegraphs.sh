#! /bin/bash

EXIT_SUCCESS=0
EXIT_ERROR=1
EXIT_FAILURE=2

IFS=$'\n'

#Funkcia nacita do premennej DIRS priecinky z test dir podla filtra
#params - $0 testdir $1 regex
function NacitajTestDir()
{

   #To ci sa podarilo otvorit testdir netestujem 
   #test prebieha uz v hlavnom programe pred volanim
   #tejto funkcie
   DIRS=`find "$1" -type d 2>/dev/null  | grep -E "$2" |sed 1d | sort`

}

if [ "$#" -ne 1 ]
then
   echo "Wrong args. Usage: makegraphs.sh test_dir " 1>&2
   exit $EXIT_FAILURE
fi


#skusime ci mozme spravit cd do testdir - ak nie = fatalne zlyhanie
cd "$1" 2> /dev/null || { echo "Unable to cd into directory $1" 1>&2 ; exit "$EXIT_FAILURE"; }

#Nacitame vsetky priecinky z TESTDIR do premennej DIRS
NacitajTestDir './' "*"

for dir in $DIRS
do
   cd "$dir" 2> /dev/null || { echo "Unable to cd into directory  $dir" 1>&2; exit "$EXIT_FAILURE"; }

      dot -Tpng prog.dot -o prog.png
      dot -Tpng prog_optimized.dot -o prog_optimized.png
      dot -Tpng functable.dot -o functable.png

   cd ../
done 
