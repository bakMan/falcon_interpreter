#! /bin/bash

EXIT_SUCCESS=0
EXIT_ERROR=1
EXIT_FAILURE=2

IFS=$'\n'

#Funkcia nacita do premennej DIRS priecinky z test dir podla filtra
#params - $0 testdir $1 regex
function NacitajTestDir()
{

   #To ci sa podarilo otvorit testdir netestujem 
   #test prebieha uz v hlavnom programe pred volanim
   #tejto funkcie
   DIRS=`find "$1" -type d 2>/dev/null  | grep -E "$2" |sed 1d | sort`

}

if [ "$#" -ne 2 ]
then
   echo "Wrong args. Usage: maketests.sh test_dir interpret" 1>&2
   exit $EXIT_FAILURE
fi

if [ ! -e "$2" ]
then
   echo "Interpret executable does not exist: $2" 1>&2
   exit $EXIT_FAILURE
fi

if [ ! -x "$2" ]
then
   echo "Second argument is not executable file: $2" 1>&2
   exit $EXIT_FAILURE
fi

INTERPRET="$(readlink -f $2)"

#skusime ci mozme spravit cd do testdir - ak nie = fatalne zlyhanie
cd "$1" 2> /dev/null || { echo "Unable to cd into directory $1" 1>&2 ; exit "$EXIT_FAILURE"; }

#Nacitame vsetky priecinky z TESTDIR do premennej DIRS
NacitajTestDir './' "*"

for dir in $DIRS
do
   cd "$dir" 2> /dev/null || { echo "Unable to cd into directory  $dir" 1>&2; exit "$EXIT_FAILURE"; }

   file="$(ls *.fal)"

   if [ "$(echo $file|wc -l)" -eq 1 ]
   then
      #if [ ! -e "stdout-expected" ]
      #then
         if [ -e "stdin-given" ] 
         then
            ulimit -t 30; $INTERPRET $file > ./stdout-expected 2> stderr-expected < stdin-given
         else
            ulimit -t 30; $INTERPRET $file > ./stdout-expected 2> stderr-expected
         fi

         echo "$?" > ./status-expected

         echo "Created test: $dir"

      #fi
   else
      echo "WARNING: No .fal file in directory:  $dir" 1>&2
   fi
   cd ../
done 
